<!DOCTYPE html>
<html lang="en">
    <head>
        @include('admin.partials.meta')
        @include('admin.partials.css')
        @yield('styles')
    </head>

    <body>
        <section id="container" >
            <div id="login-page">
                <div class="container">
                    @yield('content')
                </div>
            </div>
        </section>
        <!-- JS -->
        @include('admin.partials.js')
        @yield('javascripts')
        <!-- End JS-->
    </body>
</html>
