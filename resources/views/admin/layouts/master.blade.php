<!DOCTYPE html>
<html lang="en">
    <head>
        @include('admin.partials.meta')
        @include('admin.partials.css')
        @yield('styles')
    </head>
    <body>
        <section id="container" >
            <!--header start-->
            <header class="header black-bg">
                @include('admin.partials.header')
            </header>
            <!--header end-->
            <!--sidebar start-->
            <aside>
                @include('admin.partials.aside')
            </aside>
            <!--sidebar end-->

            <!-- **********************************************************************************************************************************************************
            MAIN CONTENT
            *********************************************************************************************************************************************************** -->
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    @yield('title')
                    <div class="row mt">
                        @include('admin.partials.flash_messages')
                        <div class="col-lg-12">
                            <p>@yield('content')</p>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->

            <!--footer start-->
            <footer class="site-footer">
                @include('admin.partials.footer')
            </footer>
            <!--footer end-->
        </section>
        <!-- js placed at the end of the document so the pages load faster -->
        @include('admin.partials.js')
        @yield('javascripts')
        
    </body>
</html>
