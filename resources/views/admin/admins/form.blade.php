{!! Form::hidden("admin_id",App\Libs\Adminauth::user()->id,['class'=>'form-control']) !!}
@php $input='name'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Name')."<em class='red'>*</em>",['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text($input,null,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@php $input='email'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Email')."<em class='red'>*</em>",['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email($input,null,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@php $input='password'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Password')."<em class='red'>*</em>",['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password($input,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@php $input='password_confirmation'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Password confirmation')."<em class='red'>*</em>",['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password($input,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>

@php $input='published'; @endphp
<div class="form-group">
    {!! Form::rawLabel($input,trans('admin.Published'),['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <label class="checkbox-inline">
            {!! Form::radio($input,1,null,[]) !!} {{trans("admin.Yes")}}
        </label>
        <label class="checkbox-inline">
            {!! Form::radio($input,0,null,[]) !!} {{trans("admin.No")}}
        </label>
    </div>
</div>
<?php
if (@$roles) {
    $final_roles = array();
    $category = "";
    foreach ($roles as $role) {
        if ($category != $role->category) {
            $category = $role->category;
            unset($value);
        }
        $key = $role->category;
        $value[$role->id] = $role->title;
        $final_roles[$key] = $value;
    }
}
?>
<div class="clearfix"></div>
@if($final_roles)
<div class="control-group">
    <label class="control-label" for="permissions">
        <h3>{{trans('admin.Permissions')}}
            <label >
                <input id="selectAll" for="Paragraphs" name="selectAll" type="checkbox" value="1">
                {{trans('admin.Select All')}}
            </label>
        </h3>
    </label>
</div>
@if($final_roles)
<div class="control-group">
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered display resize ">
            <thead>
                <tr>
                    <th width="20%">{{trans('admin.Module')}}</th>
                    <th width="80%">{{trans('admin.Actions')}}</th>
                </tr>
            </thead>
            @foreach ($final_roles as $key => $value)
            <tr>
                <td>
                    <label class="col-md-12 control-label role_head">
                        {!! Form::checkbox('heades[]',1,null,['class'=>'heads main_head_'.str_replace(' ','',$key),'id'=>$key,'for'=>str_replace(' ','',$key)]) !!} &nbsp;&nbsp;&nbsp;&nbsp;{{ucfirst($key)}}
                    </label>
                </td>
                <td>
                    @foreach ($value as $k => $v)
                    <div class="col-md-3">

                        <label class="col-md-12 control-label" for="role_{{$k}}">{!! Form::checkbox('role_list[]',$k,null,['class'=>'childs head_'.str_replace(' ','',$key),'id'=>'role_'.$k,'for'=>str_replace(' ','',$key)]) !!} {{ucfirst($v)}}</label>
                    </div>
                    @endforeach
                </td>
            </tr>
            @endforeach;
        </table>
    </div>
</div>
@endif
@endif
<div class="clearfix"></div>
@section('javascripts')
<script>
    $(function () {
        $("#selectAll").change(function () {
            if ($(this).is(':checked')) {
                $('.heads, .childs').prop('checked', true);
            } else {
                $('.heads, .childs').prop('checked', false);
            }
            console.log("yes");
        });
        $(".heads").change(function () {
            var key = $(this).attr("for");
            console.log(".head_" + key);
            $(".head_" + key).prop('checked', $(this).prop("checked"));
        });
        $(".childs").change(function () {
            var head = $(this).attr("for");
            if ($(this).is(':checked')) {
                $('.main_head_' + head).prop('checked', true);
            } else {
                if ($(".head_" + head + ":checked").size() == 0) {
                    $('.main_head_' + head).prop('checked', false);
                }
            }
        });
        $(".childs").each(function (index) {
            var head = $(this).attr("for");
            if ($(this).is(':checked')) {
                $('.main_head_' + head).prop('checked', true);
            } else {
                if ($(".head_" + head + ":checked").size() == 0) {
                    $('.main_head_' + head).prop('checked', false);
                }
            }
        });
    });
</script>
@stop