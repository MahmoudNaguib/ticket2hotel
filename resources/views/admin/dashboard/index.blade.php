@extends('admin/layouts/master')

@section('title')
<h4><i class="fa fa-angle-right"></i>{{ trans('admin.Dashboard') }}</h4>
@stop

@section('content')
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb">
    <div class="darkblue-panel pn">
        <div class="darkblue-header">
            <h4>{{trans('admin.Users')}}</h4>
        </div>
        <h1 class="mt"><i class="fa fa-users fa-x"></i></h1>
        <footer>
            <div class="centered">
                <h5>{{\App\Models\User::count()}} {{trans('admin.Users')}}</h5>
            </div>
        </footer>
    </div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb">
    <div class="darkblue-panel pn">
        <div class="darkblue-header">
            <h4>{{trans('admin.Pages')}}</h4>
        </div>
        <h1 class="mt"><i class="fa fa fa-bars fa-x"></i></h1>
        <footer>
            <div class="centered">
                <h5>{{\App\Models\Page::count()}} {{trans('admin.Pages')}}</h5>
            </div>
        </footer>
    </div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb">
    <div class="darkblue-panel pn">
        <div class="darkblue-header">
            <h4>{{trans('admin.Options')}}</h4>
        </div>
        <h1 class="mt"><i class="fa fa fa-bars fa-x"></i></h1>
        <footer>
            <div class="centered">
                <h5>{{\App\Models\Option::count()}} {{trans('admin.Options')}}</h5>
            </div>
        </footer>
    </div>
</div>


<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb">
    <div class="darkblue-panel pn">
        <div class="darkblue-header">
            <h4>{{trans('admin.Paragraphs')}}</h4>
        </div>
        <h1 class="mt"><i class="fa fa fa-bars fa-x"></i></h1>
        <footer>
            <div class="centered">
                <h5>{{\App\Models\Paragraph::count()}} {{trans('admin.Paragraphs')}}</h5>
            </div>
        </footer>
    </div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb">
    <div class="darkblue-panel pn">
        <div class="darkblue-header">
            <h4>{{trans('admin.Partners')}}</h4>
        </div>
        <h1 class="mt"><i class="fa fa fa-bars fa-x"></i></h1>
        <footer>
            <div class="centered">
                <h5>{{\App\Models\Partner::count()}} {{trans('admin.Partners')}}</h5>
            </div>
        </footer>
    </div>
</div>


<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb">
    <div class="darkblue-panel pn">
        <div class="darkblue-header">
            <h4>{{trans('admin.Contact messages')}}</h4>
        </div>
        <h1 class="mt"><i class="fa fa-envelope fa-x"></i></h1>
        <footer>
            <div class="centered">
                <h5>{{\App\Models\Contact::count()}} {{trans('admin.Contact messages')}}</h5>
            </div>
        </footer>
    </div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb">
    <div class="darkblue-panel pn">
        <div class="darkblue-header">
            <h4>{{trans('admin.Newsletter subscribers')}}</h4>
        </div>
        <h1 class="mt"><i class="fa fa-users fa-x"></i></h1>
        <footer>
            <div class="centered">
                <h5>{{\App\Models\Subscriber::count()}} {{trans('admin.Newsletter subscribers')}}</h5>
            </div>
        </footer>
    </div>
</div>

@stop
