@extends('admin.layouts.master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.View') }} {{ucfirst($module)}}</h3>
<a href="admin/{{$module}}" class="btn btn-danger"><i class="fa fa-backward"></i> {{trans('admin.Go back')}}</a>
@stop

@section('content')
@if(ACL::can('edit-'.$module))
<a href="admin/{{$module}}/edit/{{$row->id}}" class="btn btn-primary">
    <i class="fa fa-edit"></i> {{trans('admin.Edit')}}
</a>
@endif
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Title')}}</td>
            <td width="75%" class="align-left">{{$row->title}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Main image')}}</td>
            <td width="75%" class="align-left">
                @if($row->main_image)
                    <img src="uploads/small/{{$row->main_image}}">
                @endif
            </td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Meta Description')}}</td>
            <td width="75%" class="align-left">{{$row->meta_description}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Meta Keywords')}}</td>
            <td width="75%" class="align-left">{{$row->meta_keywords}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Updated at')}}</td>
            <td width="75%" class="align-left">{{$row->updated_at}}</td>
        </tr>
        @if(@$row->admin->name)
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Created by')}}</td>
            <td width="75%" class="align-left">{{@$row->admin->name}}</td>
        </tr>
        @endif
    </table>
</div>
@stop
