@extends('admin.layouts.master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.View') }} {{ucfirst($module)}}</h3>
<a href="admin/{{$module}}" class="btn btn-danger"><i class="fa fa-backward"></i> {{trans('admin.Go back')}}</a>
@stop

@section('content')
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Name')}}</td>
            <td width="75%" class="align-left">{{$row->name}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Email')}}</td>
            <td width="75%" class="align-left"><a href="mailto:{{$row->email}}">{{$row->email}}</a></td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Mobile')}}</td>
            <td width="75%" class="align-left">{{@$row->mobile}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Subject')}}</td>
            <td width="75%" class="align-left">{{$row->subject}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Message')}}</td>
            <td width="75%" class="align-left">{{$row->message}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Created at')}}</td>
            <td width="75%" class="align-left">{{$row->created_at}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Updated at')}}</td>
            <td width="75%" class="align-left">{{$row->updated_at}}</td>
        </tr>
        @if(@$row->admin->name)
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Created by')}}</td>
            <td width="75%" class="align-left">{{@$row->admin->name}}</td>
        </tr>
        @endif
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Viewed')}}</td>
            <td width="75%" class="align-left">{{($row->viewed)?trans('admin.Yes'):trans('admin.No')}}</td>
        </tr>
    </table>
</div>
@stop
