@extends('admin.layouts.master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.View') }} {{ucfirst($module)}}</h3>
<a href="admin/{{$module}}" class="btn btn-danger"><i class="fa fa-backward"></i> {{trans('admin.Go back')}}</a>
@stop

@section('content')
<div class="table-responsive">
    @php $options=\App\Models\Option::pluck('title','id') @endphp

    <h3>{{trans('admin.Agent registeration')}}</h3>

    <h4>{{trans('admin.Agent information')}}</h4>
    <p>                                 
        <strong>{{trans("admin.Name")}} : </strong> {{$row->name}}
    </p>

    <h4>{{trans('admin.Company details')}}</h4>
    <p>
        <strong>{{trans("admin.Company Name")}} : </strong> {{$row->company_name}} </p>
    <p>
        <strong>{{trans("admin.Company registeration number")}} : </strong> {{$row->company_registeration_number}} </p>
    <p>  
        <strong>{{trans("admin.IATA status")}} : </strong> {{@$options[$row->iata_status]}}</p>
    <p>
        <strong>{{trans("admin.IATA Number")}} : </strong> {{$row->iata_number}} </p>
    <p>
        <strong>{{trans("admin.Nature of business")}} : </strong> {{$row->nature_of_business_id}} </p>
    <p> 
        <strong>{{trans("admin.Website")}} : </strong> {{$row->website}} </p>
    <p>
        <strong>{{trans("admin.Address")}} : </strong> {{$row->address}} </p>
    <p> 
        <strong>{{trans("admin.Country")}} : </strong> {{@$row->country->title}} </p>
    <p> 
        <strong>{{trans("admin.City")}} : </strong> {{@$row->city->title}} </p>
    <p>  
        <strong>{{trans("admin.Pin Code")}} : </strong> {{$row->pin_code}} </p>
    <p>  


    <h4>{{trans('admin.Customer information')}}</h4>
    <p>
        <strong>{{trans("admin.First name")}} : </strong> {{$row->first_name}}
    </p>
    <p>
        <strong>{{trans("admin.Last name")}} : </strong> {{$row->last_name}}
    </p>
    <p>
        <strong>{{trans("admin.Designation")}} : </strong> {{$row->designation}}
    </p>
    <p>
        <strong>{{trans("admin.Email")}} : </strong> {{$row->email}}
    </p>
    <p>
        <strong>{{trans("admin.Phone")}} : </strong> {{$row->phone}}
    </p>
    <p>
        <strong>{{trans("admin.Mobile")}} : </strong> {{$row->mobile}}
    </p>
    <p>
        <strong>{{trans("admin.Fax")}} : </strong> {{$row->fax}}
    </p>
    <p>
        <strong>{{trans("admin.Currency")}} : </strong> {{$row->currency_id}}
    </p>
    <p>
        <strong>{{trans("admin.Time Zone")}} : </strong> {{@$options[$row->timezone]}}
    </p>

    <h4>{{trans('admin.Finance account')}}</h4>
    <h5>{{trans('admin.Accounts')}}</h5>
    <p>
        <strong>{{trans("admin.Account name")}} : </strong> {{$row->account_name}}
    </p>
    <p>
        <strong>{{trans("admin.Account email")}} : </strong> {{$row->account_email}}
    </p>
    <p>
        <strong>{{trans("admin.Account contact number")}} : </strong> {{$row->account_contact_number}}
    </p>

    <h5>{{trans('admin.Reservation')}}</h5>
    <p>
        <strong>{{trans("admin.Reservation name")}} : </strong> {{$row->reservation_name}}
    </p>
    <p>
        <strong>{{trans("admin.Reservation email")}} : </strong> {{$row->reservation_email}}
    </p>
    <p>
        <strong>{{trans("admin.Reservation contact number")}} : </strong> {{$row->reservation_contact_number}}
    </p>

    <h5>{{trans('admin.Managment')}}</h5>
    <p>
        <strong>{{trans("admin.Managment name")}} : </strong> {{$row->management_name}}
    </p>
    <p>
        <strong>{{trans("admin.Reservation email")}} : </strong> {{$row->management_email}}
    </p>
    <p>
        <strong>{{trans("admin.Reservation contact number")}} : </strong> {{$row->management_contact_number}}
    </p>
    @if($row->logo)
    <p>
        <strong>{{trans("admin.Logo")}} : </strong> 
        <a href="{{App::make("url")->to('/')}}/uploads/large/{{$row->logo}}">
            <img src="{{App::make("url")->to('/')}}/uploads/small/{{$row->logo}}" width="100">
        </a>
    </p>
    @endif
</div>
@stop
