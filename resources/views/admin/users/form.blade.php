{!! Form::hidden("admin_id",App\Libs\Adminauth::user()->id,['class'=>'form-control']) !!}

@php $input='name'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Name')."<em class='red'>*</em>",['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text($input,null,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@php $input='email'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Email')."<em class='red'>*</em>",['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email($input,null,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@php $input='mobile'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Mobile')."<em class='red'>*</em>",['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text($input,null,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@php $input='password'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Password')."<em class='red'>*</em>",['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password($input,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@php $input='password_confirmation'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Password confirmation')."<em class='red'>*</em>",['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password($input,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@if(ACL::can('publish-'.$module))
@php $input='published'; @endphp
<div class="form-group">
    {!! Form::rawLabel($input,trans('admin.Published'),['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <label class="checkbox-inline">
            {!! Form::radio($input,1,null,[]) !!} {{trans("admin.Yes")}}
        </label>
        <label class="checkbox-inline">
            {!! Form::radio($input,0,null,[]) !!} {{trans("admin.No")}}
        </label>
    </div>
</div>
@endif

@if(!@$row->confirmed)
@php $input='confirmed'; @endphp
<div class="form-group">
    {!! Form::rawLabel($input,trans('admin.Confirmed'),['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <label class="checkbox-inline">
            {!! Form::radio($input,1,null,[]) !!} {{trans("admin.Yes")}}
        </label>
        <label class="checkbox-inline">
            {!! Form::radio($input,0,null,[]) !!} {{trans("admin.No")}}
        </label>
    </div>
</div>
@endif

