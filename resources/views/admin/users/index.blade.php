@extends('admin/layouts/master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.List') }} {{ucfirst($module)}}</h3>
@stop

@section('content')
@if (!$rows->isEmpty())
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered display resize data_table">
        <thead>
            <tr>
                <th width="5%">{{trans('admin.ID')}} </th>
                <th width="25%">{{trans('admin.Name')}}</th>
                <th width="15%">{{trans('admin.Company name')}}</th>
                <th width="15%">{{trans('admin.Created at')}}</th>
                @if(isset($rows[0]->published))
                @if(ACL::can('publish-'.$module))
                <th width="5%">{{trans('admin.Published')}}?</th>
                @endif
                @endif
                <th width="15%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rows as $row)
            <tr>
                <td class="center">{{$row->id}}</td>
                <td class="center">{{$row->name}}</td>
                <td class="center">{{$row->company_name}}</td>
                <td class="center">{{$row->created_at}}</td>
                @if(isset($row->published))
                @if(ACL::can('publish-'.$module))
                <td class="center">
                    @if (@$row->published == 1)
                    <a href="admin/{{$module}}/publish/0/{{@$row->id}}"><img src="assets/admin/img/check.png"></a>
                    @else
                    <a href="admin/{{$module}}/publish/1/{{@$row->id}}"><img src="assets/admin/img/close.png"></a>
                    @endif
                </td>
                @endif
                @endif
                <td class="center">
                    @if(ACL::can('view-'.$module))
                    <a class="btn btn-primary btn-xs" href="admin/{{$module}}/view/{{@$row->id}}" title="{{trans('admin.View')}}">
                        <i class="fa fa-eye"></i>
                    </a>
                    @endif
                    @if(ACL::can('delete-'.$module))
                    <a class="btn btn-danger btn-xs" href="admin/{{$module}}/delete/{{@$row->id}}" title="{{trans('admin.Delete')}}" data-title="{{trans('admin.Confirmation message')}}" data-confirm="{{trans('admin.Are you sure you want to delete this user')}}?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{!! Form::close() !!}
@else
{{trans("admin.There is no results")}}
@endif

@stop
