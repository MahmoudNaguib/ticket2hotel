@extends('admin.layouts.login')

@section('content')
<form class="form-login" method="post" enctype="multipart/form-data" id="form">
    <h2 class="form-login-heading">{{trans("admin.forgot password")}}</h2>
    <div class="login-wrap">
        @include('admin.partials.flash_messages')
        {!! csrf_field() !!}
        {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>trans('admin.Enter your email')]) !!}
        @foreach($errors->get('email') as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
        <br>
        <button  name="login" class="btn btn-theme btn-block" type="submit">
            <i class="fa fa-lock"></i> {{trans('admin.Send')}} </button>
        <hr>
        <a href="admin/auth/login" class="btn btn-theme btn-block">{{trans('admin.Login')}}</a>
    </div>
</form>
@stop
