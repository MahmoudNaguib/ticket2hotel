@extends('admin.layouts.login')

@section('content')
<form class="form-login" method="post" enctype="multipart/form-data" id="form">
    <h2 class="form-login-heading">{{trans("admin.Sign in")}}</h2>
    <div class="login-wrap">
        @include('admin.partials.flash_messages')
        {!! csrf_field() !!}
        {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>trans('admin.Enter your email')]) !!}
        @foreach($errors->get('email') as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
        <br>
        {!! Form::password('password',['class'=>'form-control','placeholder'=>trans('admin.Enter your password')]) !!}
        @foreach($errors->get('password') as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
        <br>
        <button  name="login" class="btn btn-theme btn-block" type="submit">
            <i class="fa fa-lock"></i> {{trans('admin.Login')}} </button>
        <hr>
        <a href="admin/auth/forgot-password" class="btn btn-theme btn-block">{{trans('admin.Forgot password')}}</a>
    </div>
</form>
@stop
