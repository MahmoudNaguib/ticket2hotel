@extends('admin/layouts/master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.List') }}  {{trans("admin.Roles")}}</h3>
@stop

@section('content')
@if(ACL::can('create-'.$module))
<a href="admin/{{$module}}/create" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('admin.Create')}}</a>
@endif
@if (!$rows->isEmpty())
{!! Form::open(['url' => 'admin/'.$module.'/delete', 'method' => 'post','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered display resize data_table">
        <thead>
            <tr>
                <th width="5%">{{trans('admin.ID')}} </th>
                <th width="20%">{{trans('admin.Title')}}</th>
                <th width="15%">{{trans('admin.Created at')}}</th>
                <th width="20%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rows as $row)
            <tr>
                <td class="center">{{$row->id}}</td>
                <td class="center">{{$row->title}}</td>
                <td class="center">{{$row->created_at}}</td>
                <td class="center">
                    @if(ACL::can('edit-'.$module))
                    <a class="btn btn-success btn-xs" href="admin/{{$module}}/edit/{{$row->id}}" title="{{trans('admin.Edit')}}">
                        <i class="fa fa-edit"></i>
                    </a>
                    @endif
                    @if(ACL::can('view-'.$module))
                    <a class="btn btn-primary btn-xs" href="admin/{{$module}}/view/{{$row->id}}" title="{{trans('admin.View')}}">
                        <i class="fa fa-eye"></i>
                    </a>
                    @endif
                    @if(ACL::can('delete-'.$module))
                    <a class="btn btn-danger btn-xs" href="admin/{{$module}}/delete/{{$row->id}}" title="{{trans('admin.Delete')}}" data-title="{{trans('admin.Confirmation message')}}" data-confirm="{{trans('admin.Are you sure you want to delete this role')}}?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{!! Form::close() !!}
@else
{{trans("admin.There is no results")}}
@endif

@stop
