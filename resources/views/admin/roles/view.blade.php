@extends('admin.layouts.master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.View') }} {{trans("admin.Role")}}</h3>
<a href="admin/{{$module}}" class="btn btn-danger"><i class="fa fa-backward"></i> {{trans('admin.Go back')}}</a><br><br>
@stop

@section('content')
@if(ACL::can('edit-'.$module))
<a href="admin/{{$module}}/edit/{{$row->id}}" class="btn btn-primary">
    <i class="fa fa-edit"></i> {{trans('admin.Edit')}}
</a><br><br>
@endif
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Title')}}</td>
            <td width="75%" class="align-left">{{$row->title}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Slug')}}</td>
            <td width="75%" class="align-left">{{$row->title}}</td>
        </tr>
        <tr>
            <td width="25%" class="align-left">{{trans('admin.Category')}}</td>
            <td width="75%" class="align-left">{{$row->category}}</td>
        </tr>

        <tr>
            <td width="25%" class="align-left">{{trans('admin.Created at')}}</td>
            <td width="75%" class="align-left">{{$row->created_at}}</td>
        </tr>
    </table>
</div>
@stop
