@extends('admin.layouts.master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.View') }} {{ucfirst($module)}}</h3>
<a href="admin/{{$module}}" class="btn btn-danger"><i class="fa fa-backward"></i> {{trans('admin.Go back')}}</a>
@stop

@section('content')
<div class="table-responsive">
    <h4><strong>{{trans("admin.Group Information")}}</strong></h4>
    @if(@$row->group_name)                                              
    <p>
        <label>
            <strong>{{trans("admin.Group name")}} </strong> 
            {{$row->group_name}} 
        </label>
    </p>
    @endif

    @if(@$row->group_type)                                              
    <p>
        <label>
            <strong>{{trans("admin.Group type")}} </strong> 
            {{$row->group_type}} 
        </label>
    </p>
    @endif

    @if(@$row->nationality)                                              
    <p>
        <label>
            <strong>{{trans("admin.Nationality")}} </strong> 
            {{$row->nationality}} 
        </label>
    </p>
    @endif

    @if(@$row->destination)                                              
    <p>
    <h4><strong>{{trans("admin.Destination")}}</strong></h4> 
    @php $destination=json_decode($row->destination); @endphp
    @for($i=0; $i<sizeof(@$destination->country);$i++)
        {{trans('admin.Country')}} : {{@$destination->country[$i]}} <br>
        {{trans('admin.City')}} : {{@$destination->city[$i]}} <br>
        {{trans('admin.CheckIn')}} : {{@$destination->check_in[$i]}} <br>
        {{trans('admin.CheckOut')}} : {{@$destination->check_out[$i]}} <br>
        ---------------------------------------- <br>
        @endfor    
        @endif

        <h4><strong>{{trans("admin.Types of Services")}}</strong></h4> 
        @if(@$row->flight_checkbox) 
        <h5><strong><u>{{trans('admin.Flight')}}</u></strong></h5>
        @if(@$row->flight)
        @php $flight=json_decode($row->flight); @endphp
        @foreach($flight as $k=>$v)
        <p>
            <strong>{{clean($k)}}</strong>
            {{$v}}
        </p>
        @endforeach
        @endif    
        @endif

        @if(@$row->hotel_checkbox) 
        <h5><strong><u>{{trans('admin.Hotel')}}</u></strong></h5>
        @if(@$row->hotel)
        @php $hotel=json_decode($row->hotel); @endphp
        @foreach($hotel as $k=>$v)
        <p>
            <strong>{{clean($k)}}</strong>
            {{$v}}
        </p>
        @endforeach
        @endif    
        @endif

        @if(@$row->transfer_checkbox) 
        <h5><strong><u>{{trans('admin.Transfer')}}</u></strong></h5>
        @if(@$row->transfer)
        @php $transfer=json_decode($row->transfer); @endphp
        @foreach($transfer as $k=>$v)
        <p>
            <strong>{{clean($k)}}</strong>
            {{$v}}
        </p>
        @endforeach
        @endif    
        @endif

        @if(@$row->tours_checkbox) 
        <h5><strong><u>{{trans('admin.Tours')}}</u></strong></h5>
        @if(@$row->tours)
        @php $tours=json_decode($row->tours); @endphp
        @foreach($tours as $k=>$v)
        <p>
            <strong>{{clean($k)}}</strong>
            {{$v}}
        </p>
        @endforeach
        @endif    
        @endif


        @if(@$row->others_checkbox) 
        <h5><strong><u>{{trans('admin.Others')}}</u></strong></h5>
        @if(@$row->others)
        @php $others=json_decode($row->others); @endphp
        @foreach($others as $k=>$v)
        <p>
            <strong>{{clean($k)}}</strong>
            {{$v}}
        </p>
        @endforeach
        @endif    
        @endif

        <h4><strong>{{trans("admin.Contact information")}}</strong></h4>
        @if(@$row->company_name)                                                                            
        <p><label><strong>{{trans("admin.Company name")}} </strong> {{$row->company_name}} </label></p>
        @endif 
        @if(@$row->contact_name)                                                                            
        <p><label><strong>{{trans("admin.Contact name")}} </strong> {{$row->contact_name}} </label></p>
        @endif 

        @if(@$row->contact_mobile)                                                                            
        <p><label><strong>{{trans("admin.Contact mobile")}} </strong> {{$row->contact_mobile}} </label></p>
        @endif

        @if(@$row->contact_email)                                                                            
        <p><label><strong>{{trans("admin.Contact email")}} </strong> {{$row->contact_email}} </label></p>
        @endif 
</div>
@stop
