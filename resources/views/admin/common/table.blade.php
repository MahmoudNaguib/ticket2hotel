<div class="row">
    <div class="col-md-offset-8 col-md-4">
        <form class="navbar-form" role="search">
            <div class="input-group pull-right">
                <input type="text" class="form-control" placeholder="{{trans("admin.Search by title")}}" name="q" value="{{Request::input("q")}}">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            @if(Request::input("q"))
            <div class="input-group pull-left">
                <div class="input-group-btn">
                    <a class="btn btn-default" href="admin/{{$module}}">{{trans("admin.Disable search")}}</a>
                </div>
            </div>
            @endif
        </form>
        <br><br>
    </div>
</div>

@if (!$rows->isEmpty())

{!! Form::open(['url' => 'admin/'.$module.'/delete', 'method' => 'post','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered display resize">
        <thead>
            <tr>
                @if(ACL::can('delete-'.$module))
                <th width="5%"><input type="checkbox" id="checkall" /></th>
                @endif
                <th width="35%">{{trans('admin.Title')}}</th>
                <th width="15%">{{trans('admin.Created at')}}</th>
                @if(isset($rows[0]->published))
                @if(ACL::can('publish-'.$module))
                <th width="5%">{{trans('admin.Published')}}?</th>
                @endif
                @endif
                <th width="20%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rows as $row)
            <tr>
                @if(ACL::can('delete-'.$module))
                <td class="center"><input type="checkbox" class="checkthis" name="id[]" value="{{$row->id}}"/></td>
                @endif
                <td class="center">{{$row->title }}</td>
                <td class="center">{{$row->created_at}}</td>
                @if(isset($row->published))
                @if(ACL::can('publish-'.$module))
                <td class="center">
                    @if ($row->published == 1)
                    <a href="admin/{{$module}}/publish/0/{{$row->id}}"><img src="assets/admin/img/check.png"></a>
                    @else
                    <a href="admin/{{$module}}/publish/1/{{$row->id}}"><img src="assets/admin/img/close.png"></a>
                    @endif
                </td>
                @endif
                @endif
                <td class="center">
                    @if(ACL::can('edit-'.$module))
                    <a class="btn btn-success btn-xs" href="admin/{{$module}}/edit/{{$row->id}}" title="{{trans('admin.Edit')}}">
                        <i class="fa fa-edit"></i>
                    </a>
                    @endif
                    @if(ACL::can('view-'.$module))
                    <a class="btn btn-primary btn-xs" href="admin/{{$module}}/view/{{$row->id}}" title="{{trans('admin.View')}}">
                        <i class="fa fa-eye"></i>
                    </a>
                    @endif
                    @if(ACL::can('delete-'.$module))
                    <a class="btn btn-danger btn-xs" href="admin/{{$module}}/delete/{{$row->id}}" title="{{trans('admin.Delete')}}" data-title="{{trans('admin.Confirmation message')}}" data-confirm="{{trans('admin.Are you sure you want to delete this item')}}?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                    @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div >
    <div class="pull-left">
        @if(ACL::can('delete-'.$module))
        {!! Form::submit(trans('admin.Delete selected') ,['class' => 'btn btn-danger']) !!}
        @endif
    </div>
    <div class="pull-right">
        {!! $rows->appends(['q'=>@Request::input('q')])->render() !!}
    </div>
</div>
{!! Form::close() !!}
@else
{{trans("admin.There is no results")}}
@endif
