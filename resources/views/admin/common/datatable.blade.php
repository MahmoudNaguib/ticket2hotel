@if (!$rows->isEmpty())
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered display resize data_table">
        <thead>
            <tr>
                <th width="5%">{{trans('admin.ID')}} </th>
                <th width="35%">{{trans('admin.Title')}}</th>
                <th width="15%">{{trans('admin.Created at')}}</th>
                @if(isset($rows[0]->published))
                @if(ACL::can('publish-'.$module))
                <th width="5%">{{trans('admin.Published')}}?</th>
                @endif
                @endif
                <th width="20%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rows as $row)
            <tr>
                <td class="center">{{$row->id}}</td>
                <td class="center">{{$row->title }}</td>
                <td class="center">{{$row->created_at}}</td>
                @if(isset($row->published))
                @if(ACL::can('publish-'.$module))
                <td class="center">
                    @if ($row->published == 1)
                    <a href="admin/{{$module}}/publish/0/{{$row->id}}"><img src="assets/admin/img/check.png"></a>
                    @else
                    <a href="admin/{{$module}}/publish/1/{{$row->id}}"><img src="assets/admin/img/close.png"></a>
                    @endif
                </td>
                @endif
                @endif
                <td class="center">
                    @if(ACL::can('edit-'.$module))
                    <a class="btn btn-success btn-xs" href="admin/{{$module}}/edit/{{$row->id}}" title="{{trans('admin.Edit')}}">
                        <i class="fa fa-edit"></i>
                    </a>
                    @endif
                    @if(ACL::can('view-'.$module))
                    <a class="btn btn-primary btn-xs" href="admin/{{$module}}/view/{{$row->id}}" title="{{trans('admin.View')}}">
                        <i class="fa fa-eye"></i>
                    </a>
                    @endif
                    @if(ACL::can('delete-'.$module))
                    <a class="btn btn-danger btn-xs" href="admin/{{$module}}/delete/{{$row->id}}" title="{{trans('admin.Delete')}}" data-title="{{trans('admin.Confirmation message')}}" data-confirm="{{trans('admin.Are you sure you want to delete this item')}}?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
