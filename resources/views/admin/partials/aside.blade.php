<div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
        <h5 class="centered">{{trans("admin.Welcome")}} {{App\Libs\Adminauth::user()->name}}</h5>
        <li>
            <a href="admin/dashboard" class="{{(Request::is('admin/dashboard*'))?"active":""}}">
                <i class="fa fa-dashboard"></i>
                <span>{{trans('admin.Dashboard')}}</span>
            </a>
        </li>
        @if(Adminauth::user()->super_admin)
        <li>
            <a href="admin/configs/edit" class="{{(Request::is('admin/configs*'))?"active":""}}">
                <i class="fa fa-cogs"></i>
                <span>{{trans('admin.Settings')}}</span>
            </a>
        </li>   
        @endif

        @if(Adminauth::user()->super_admin)
        <li>
            <a href="admin/admins" class="{{(Request::is('admin/admins*'))?"active":""}}">
                <i class="fa fa-users"></i>
                <span>{{trans('admin.Administrators')}}</span>
            </a>
        </li>
        @endif
        
        @if(ACL::can('view-users'))
        <li>
            <a href="admin/users" class="{{(Request::is('admin/users*'))?"active":""}}">
                <i class="fa fa-user"></i>
                <span>{{trans('admin.Users requests')}}</span>
            </a>
        </li>
        @endif
        
        @if(ACL::can('view-pages'))
        <li>
            <a href="admin/pages" class="{{(Request::is('admin/pages*'))?"active":""}}">
                <i class="fa fa-bars"></i>
                <span>{{trans('admin.Pages')}}</span>
            </a>
        </li>
        @endif
        
        @if(ACL::can('view-options'))
        <li>
            <a href="admin/options" class="{{(Request::is('admin/options*'))?"active":""}}">
                <i class="fa fa-bars"></i>
                <span>{{trans('admin.Options')}}</span>
            </a>
        </li>
        @endif
        
        @if(ACL::can('view-paragraphs'))
        <li>
            <a href="admin/paragraphs" class="{{(Request::is('admin/paragraphs*'))?"active":""}}">
                <i class="fa fa-bars"></i>
                <span>{{trans('admin.Paragraphs')}}</span>
            </a>
        </li>
        @endif
        
        @if(ACL::can('view-partners') || ACL::can('create-partners'))
        <li>
            <a href="admin/partners" class="{{(Request::is('admin/partners*'))?"active":""}}">
                <i class="fa fa-bars"></i>
                <span>{{trans('admin.Partners')}}</span>
            </a>
        </li>
        @endif
        @if(ACL::can('view-charters'))
        <li>
            <a href="admin/charters" class="{{(Request::is('admin/charters*'))?"active":""}}">
                <i class="fa fa-envelope"></i>
                <span>{{trans('admin.Charters requests')}}</span>
            </a>
        </li>
        @endif
        
        @if(ACL::can('view-groups'))
        <li>
            <a href="admin/groups" class="{{(Request::is('admin/groups*'))?"active":""}}">
                <i class="fa fa-envelope"></i>
                <span>{{trans('admin.Groups requests')}}</span>
            </a>
        </li>
        @endif

        @if(ACL::can('view-contacts'))
        <li>
            <a href="admin/contacts" class="{{(Request::is('admin/contacts*'))?"active":""}}">
                <i class="fa fa-envelope"></i>
                <span>{{trans('admin.Contact messages')}}</span>
            </a>
        </li>
        @endif

        @if(ACL::can('view-subscribers') || ACL::can('create-subscribers'))
        <li>
            <a href="admin/subscribers" class="{{(Request::is('admin/subscribers*'))?"active":""}}">
                <i class="fa fa-users"></i>
                <span>{{trans('admin.Subscribers')}}</span>
            </a>
        </li>
        @endif
        @if(ACL::can('view-slides') || ACL::can('create-slides'))
        <li>
            <a href="admin/slides" class="{{(Request::is('admin/slides*'))?"active":""}}">
                <i class="fa fa-bars"></i>
                <span>{{trans('admin.Slides')}}</span>
            </a>
        </li>
        @endif
        
    </ul>
    <!-- sidebar menu end-->
</div>