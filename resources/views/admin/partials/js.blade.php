<script src="assets/admin/js/jquery.js"></script>
<script src="assets/admin/js/bootstrap.min.js"></script>
<!--Plugins-->
<script type="text/javascript" src="assets/admin/plugins/dcjqaccordion/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/admin/plugins/scrollTo/js/jquery.scrollTo.min.js" type="text/javascript"></script>
<script src="assets/admin/plugins/nicescroll/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="assets/admin/plugins/jquery.validation/js/jquery.validate.js" type="text/javascript"></script>
<script src="assets/admin/plugins/jquery.validation/js/additional-methods.js" type="text/javascript"></script>
<script src="assets/admin/plugins/jquery-ui/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<script src="assets/admin/plugins/chosen/js/chosen.jquery.min.js" type="text/javascript"></script>
<script src="assets/admin/plugins/datatable/js/datatable.jquery1.10.10.js" type="text/javascript"></script>
<script src="assets/admin/plugins/datatable/js/datatable_bootstrap.js" type="text/javascript"></script>

<script src="assets/admin/plugins/switch/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="assets/admin/plugins/timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="assets/admin/plugins/daterangepicker/js/moment.min.js" type="text/javascript"></script>
<script src="assets/admin/plugins/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
<script src="assets/admin/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/admin/plugins/colorpicker/js/colorpicker.js"></script>
<script src="assets/admin/plugins/colorpicker/js/jscolor.js"></script>
<script src="assets/admin/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="assets/admin/plugins/bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>
<script src="assets/admin/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
<script src="assets/admin/plugins/jquery.geocomplete/js/jquery.geocomplete.js" type="text/javascript"></script>
<!--common script for all pages-->
@include("admin.partials.froala.js")
<script src="assets/admin/js/common-scripts.js"></script>
<script src="assets/admin/js/custom.js"></script>
