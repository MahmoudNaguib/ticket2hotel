<div class="text-center">
    {{trans("admin.Copyright")}} {{date("Y")}} © {{$configs['site_title'] or env('SITE_TITLE')}}
    <a href="blank.html#" class="go-top">
        <i class="fa fa-angle-up"></i>
    </a>
</div>