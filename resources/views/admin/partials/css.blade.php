<link href="assets/admin/css/bootstrap.css" rel="stylesheet">
<link href="assets/admin/css/style.css" rel="stylesheet">
<link href="assets/admin/css/style-responsive.css" rel="stylesheet">
<!--Plugins-->
<link href="assets/admin/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="assets/admin/plugins/chosen/css/bootstrap-chosen.css" rel="stylesheet" />
<link href="assets/admin/plugins/datatable/css/datatable_bootstrap.css" rel="stylesheet" />
<link href="assets/admin/plugins/timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" />
<link href="assets/admin/plugins/daterangepicker/css/daterangepicker.css" rel="stylesheet" />
<link href="assets/admin/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="assets/admin/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="assets/admin/plugins/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
<link href="assets/admin/plugins/summernote/css/summernote.css" rel="stylesheet" />
<link rel="stylesheet" media="screen" type="text/css" href="assets/admin/plugins/colorpicker/css/colorpicker.css" />
@include("admin.partials.froala.css")
<!--end Plugins-->
<link href="assets/admin/css/custom.css" rel="stylesheet">
@yield('styles')
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

