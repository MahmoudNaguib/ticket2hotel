<div class="sidebar-toggle-box">
    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
</div>
<!--logo start-->
<a href="admin/dashboard" class="logo"><b><span>{{$configs['site_title'] or env('SITE_TITLE')}}</span></b></a>
<!--logo end-->
<div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <!--    <ul class="nav top-menu">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                    <i class="fa fa-tasks"></i>
                    <span class="badge bg-theme">1</span>
                </a>
                <ul class="dropdown-menu extended tasks-bar">
                    <div class="notify-arrow notify-arrow-green"></div>
                    <li>
                        <p class="green">You have 1 pending tasks</p>
                    </li>
                    <li>
                        <a href="index.html#">
                            <div class="task-info">
                                <div class="desc">Dashio Admin Panel</div>
                                <div class="percent">40%</div>
                            </div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Complete (success)</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="external">
                        <a href="#">See All Tasks</a>
                    </li>
                </ul>
            </li>
            <li id="header_inbox_bar" class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-theme">1</span>
                </a>
                <ul class="dropdown-menu extended inbox">
                    <div class="notify-arrow notify-arrow-green"></div>
                    <li>
                        <p class="green">You have 1 new messages</p>
                    </li>
                    <li>
                        <a href="index.html#">
                            <span class="photo"><img alt="avatar" src="assets/admin/img/ui-zac.jpg"></span>
                            <span class="subject">
                                <span class="from">Zac Snider</span>
                                <span class="time">Just now</span>
                            </span>
                            <span class="message">
                                Hi mate, how is everything?
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="index.html#">See all messages</a>
                    </li>
                </ul>
            </li>
            <li id="header_notification_bar" class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                    <i class="fa fa-bell-o"></i>
                    <span class="badge bg-warning">1</span>
                </a>
                <ul class="dropdown-menu extended notification">
                    <div class="notify-arrow notify-arrow-yellow"></div>
                    <li>
                        <p class="yellow">You have 1 new notifications</p>
                    </li>
                    <li>
                        <a href="index.html#">
                            <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                            Server Overloaded.
                            <span class="small italic">4 mins.</span>
                        </a>
                    </li>
                    <li>
                        <a href="index.html#">See all notifications</a>
                    </li>
                </ul>
            </li>
             notification dropdown end
        </ul>-->
    <!--  notification end -->
</div>
<div class="top-menu">
    <ul class="nav pull-right top-menu" style="margin-top: 15px;margin-left: 30px;">
        <li class="dropdown">
            <a href="{{App::make("url")->to('/')}}/" class="dropdown-toggle" data-toggle="dropdown">{{trans("admin.Welcome")}} {{App\Libs\Adminauth::user()->name}} <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="admin/admins/edit-account">{{trans('admin.Edit account')}}</a></li>
                <li><a href="admin/admins/change-password">{{trans('admin.Change password')}}</a></li>
                <li><a class="logout" href="admin/auth/logout" data-title="{{trans('admin.Confirmation message')}}" data-confirm="{{trans('admin.Are you sure you want to logout')}}?">{{trans('admin.Logout')}}</a></li>
            </ul>
        </li>
        <li></li>
    </ul>
</div>