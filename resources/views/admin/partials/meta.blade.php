<base href="{{App::make("url")->to('/')}}/" />
<title>{{$configs['site_title'] or env('SITE_TITLE')}} - {{trans('admin.Admin panel')}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">