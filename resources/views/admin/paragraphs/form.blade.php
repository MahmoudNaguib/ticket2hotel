{!! Form::hidden("admin_id",App\Libs\Adminauth::user()->id,['class'=>'form-control']) !!}
@php $input='title'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Title')."<em class='red'>*</em>",['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text($input,null,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>

@php $input='content'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Content'),['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea($input,html_entity_decode($row->$input),['class'=>'form-control froala']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@php $input='main_image'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Main image'),['class' => 'col-md-2 control-label']) !!} {{trans("admin.jpg,png,jpeg")}}, {{trans("front.Allowed max file size 4MB")}} {{trans("admin.Recommended resolution 644x339")}}
    <div class="col-md-4">
        {!! Form::file($input,['class'=>'form-control fileinput','accept'=>'image/*','data-show-preview'=>'false','data-allowed-file-extensions'=>'["jpg", "png","jpeg"]']) !!}
        {!!viewValue($row->$input,'image')!!}
        @if($row->$input)
        <a class="btn btn-danger" data-confirm="{{trans('admin.Are you sure you want to delete this image')}}?" href="admin/{{$module}}/delete-main-image/{{$row->id}}" data-title="{{trans('admin.Confirmation message')}}">
            <i class="fa fa-trash-o"></i> {{trans("admin.Delete")}}
        </a>
        @endif
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@section('javascripts')
<script>
    $('.style-form').validate({
        rules: {
            'main_image': {
                extension: "jpg,jpeg,png",
                filesize: 4000000
            }
        },
        messages: {
            'main_image': {
                extension: "Only png,jpg,jpeg file is allowed!",
                filesize: "The file(s) selected exceed the file size limit"f
            }
        }
    });
</script>
@stop