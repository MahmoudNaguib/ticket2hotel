{!! Form::hidden("admin_id",App\Libs\Adminauth::user()->id,['class'=>'form-control']) !!}
@php $input='title'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Title')."<em class='red'>*</em>",['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text($input,null,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>

@php $input='main_image'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Main image')."<em class='red'>*</em>",['class' => 'col-md-2 control-label']) !!} {{trans("front.jpg,png,jpeg")}}, {{trans("front.Allowed max file size 4MB")}} {{trans("admin.Recommended resolution")}} 600x480
    <div class="col-md-4">
        {!! Form::file($input,['class'=>'form-control fileinput','accept'=>'image/*','data-show-preview'=>'false','data-allowed-file-extensions'=>'["jpg", "png","jpeg"]']) !!}
        {!!viewValue($row->$input,'image')!!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>

@if(ACL::can('publish-'.$module))
@php $input='published'; @endphp
<div class="form-group">
    {!! Form::rawLabel($input,trans('admin.Published'),['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <label class="checkbox-inline">
            {!! Form::radio($input,1,null,[]) !!} {{trans("admin.Yes")}}
        </label>
        <label class="checkbox-inline">
            {!! Form::radio($input,0,null,[]) !!} {{trans("admin.No")}}
        </label>
    </div>
</div>
@endif

@section('javascripts')
<script>
    $('.style-form').validate({rules: {
        'main_image': {
        extension: "jpg,jpeg,png",
                filesize: 4000000
                @if (Request::is('admin/'.$module.'/create*'))
                , required: true
                @endif
        }
    },
        messages: {
        'main_image': {
        extension: "Only png,jpg,jpeg file is allowed!",
                filesize: "The file(s) selected exceed the file size limit"
                @if (Request::is('admin/'.$module.'/create*'))
                , required: "The field is required"
                @endif
        }
    }
    });
</script>
@stop