{!! Form::hidden("admin_id",App\Libs\Adminauth::user()->id,['class'=>'form-control']) !!}
@php $input='email'; @endphp
<div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
    {!! Form::rawLabel($input,trans('admin.Email')."<em class='red'>*</em>",['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text($input,null,['class'=>'form-control']) !!}
        @foreach($errors->get($input) as $message)
        <span class = 'help-inline text-danger'>{{ $message }}</span>
        @endforeach
    </div>
</div>
@if(ACL::can('publish-'.$module))
@php $input='published'; @endphp
<div class="form-group">
    {!! Form::rawLabel($input,trans('admin.Published'),['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <label class="checkbox-inline">
            {!! Form::radio($input,1,null,[]) !!} {{trans("admin.Yes")}}
        </label>
        <label class="checkbox-inline">
            {!! Form::radio($input,0,null,[]) !!} {{trans("admin.No")}}
        </label>
    </div>
</div>
@endif