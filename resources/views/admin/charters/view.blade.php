@extends('admin.layouts.master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.View') }} {{ucfirst($module)}}</h3>
<a href="admin/{{$module}}" class="btn btn-danger"><i class="fa fa-backward"></i> {{trans('admin.Go back')}}</a>
@stop

@section('content')
<div class="table-responsive">
    @php
    $types=\App\Models\Charter::types();
    $etdOptions=\App\Models\Charter::etdOptions();
    @endphp
    @if($row->type)                                              
    <p><label><strong>{{trans("admin.Type")}} </strong> {{@$types[$row->type]}} </label></p>
    @endif     

    @if($row->no_of_passengers)                                                                            
    <p><label><strong>{{trans("admin.Number of passengers")}} </strong> {{$row->no_of_passengers}} </label></p>
    @endif 

    @if($row->total_weight)                                                                            
    <p><label><strong>{{trans("admin.Total weight in KG")}} </strong> {{$row->total_weight}} </label></p>
    @endif 

    @if($row->from)                                                                            
    <p><label><strong>{{trans("admin.From")}} </strong> {{$row->from}} </label></p>
    @endif 
    @if($row->to)                                                                            
    <p><label><strong>{{trans("admin.To")}} </strong> {{$row->to}} </label></p>
    @endif 
    @if($row->date_of_operation)                                                                            
    <p><label><strong>{{trans("admin.Date of operation")}} </strong> {{$row->date_of_operation}} </label></p>
    @endif 
    @if($row->prefered_etd)                                                                            
    <p><label><strong>{{trans("admin.Prefered ETD")}} </strong>{{@$etdOptions[$row->prefered_etd]}} </label></p>
    @endif 

    @if($row->notes)                                                                            
    <p><label><strong>{{trans("admin.Notes")}} </strong> {{$row->notes}} </label></p>
    @endif 

    @if($row->contact_email)                                                                            
    <p><label><strong>{{trans("admin.Contact email")}} </strong> {{$row->contact_email}} </label></p>
    @endif 

    @if($row->contact_mobile)                                                                            
    <p><label><strong>{{trans("admin.Contact mobile")}} </strong> {{$row->contact_mobile}} </label></p>
    @endif

    @if($row->contact_name)                                                                            
    <p><label><strong>{{trans("admin.Contact name")}} </strong> {{$row->contact_name}} </label></p>
    @endif 
</div>
@stop
