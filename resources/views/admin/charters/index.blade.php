@extends('admin/layouts/master')

@section('title')
<h3><i class="fa fa-angle-right"></i>{{ trans('admin.List') }} {{ucfirst($module)}}</h3>
@stop

@section('content')
@if(ACL::can('export-'.$module))
<a href="admin/{{$module}}/export" class="btn btn-default">{{trans('admin.Export')}}</a>
@endif



@if(ACL::can('view-'.$module))
@if (!$rows->isEmpty())
{!! Form::open(['url' => 'admin/'.$module.'/delete', 'method' => 'post','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered display resize data_table">
        <thead>
            <tr>
                <th width="5%">{{trans('admin.ID')}} </th>
                <th width="10%">{{trans('admin.Type')}}</th>
                <th width="10%">{{trans('admin.Contact name')}}</th>
                <th width="10%">{{trans('admin.Contact mobile')}}</th>
                <th width="10%">{{trans('admin.Contact email')}}</th>
                <th width="10%">{{trans('admin.Viewed')}}</th>
                <th width="15%">{{trans('admin.Created at')}}</th>
                <th width="15%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @php
$types=\App\Models\Charter::types();
$etdOptions=\App\Models\Charter::etdOptions();
@endphp
            @foreach ($rows as $row)
            <tr>
                <td class="center">{{$row->id}}</td>
                <td class="center">{{@$types[$row->type]}}</td>
                <td class="center">{{$row->contact_name}}</td>
                <td class="center">{{$row->contact_mobile}}</td>
                <td class="center"><a href="mailto:{{$row->contact_email}}">{{$row->contact_email}}</a></td>
                <td class="center">{{($row->viewed)?trans("admin.Yes"):trans("admin.No")}}</td>
                <td class="center">{{$row->created_at}}</td>
                <td class="center">
                    @if(ACL::can('view-'.$module))
                    <a class="btn btn-primary btn-xs" href="admin/{{$module}}/view/{{$row->id}}" title="{{trans('admin.View')}}">
                        <i class="fa fa-eye"></i>
                    </a>
                    @endif
                    @if(ACL::can('delete-'.$module))
                    <a class="btn btn-danger btn-xs" href="admin/{{$module}}/delete/{{$row->id}}" title="{{trans('admin.Delete')}}" data-title="{{trans('admin.Confirmation message')}}" data-confirm="{{trans('admin.Are you sure you want to delete this contact message')}}?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{!! Form::close() !!}
@else
{{trans("admin.There is no results")}}
@endif
@endif
@stop
