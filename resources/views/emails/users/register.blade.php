<html><head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>{{trans("email.Agent registeration")}}</title>
        <style>
            /* -------------------------------------
                GLOBAL RESETS
            ------------------------------------- */
            img {
                border: none;
                -ms-interpolation-mode: bicubic;
                max-width: 100%; }

            body {
                background-color: #f6f6f6;
                font-family: sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%; }

            table {
                border-collapse: separate;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                width: 100%; }
            table td {
                font-family: sans-serif;
                font-size: 14px;
                vertical-align: top; }

            /* -------------------------------------
                BODY & CONTAINER
            ------------------------------------- */

            .body {
                background-color: #f6f6f6;
                width: 100%; }

            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
                display: block;
                Margin: 0 auto !important;
                /* makes it centered */
                max-width: 580px;
                padding: 10px;
                width: auto !important;
                width: 580px; }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                box-sizing: border-box;
                display: block;
                Margin: 0 auto;
                max-width: 580px;
                padding: 10px; }

            /* -------------------------------------
                HEADER, FOOTER, MAIN
            ------------------------------------- */
            .main {
                background: #fff;
                border-radius: 3px;
                width: 100%; }

            .wrapper {
                box-sizing: border-box;
                padding: 20px; }

            .footer {
                clear: both;
                padding-top: 10px;
                text-align: center;
                width: 100%; }
            .footer td,
            .footer p,
            .footer span,
            .footer a {
                color: #999999;
                font-size: 12px;
                text-align: center; }

            /* -------------------------------------
                TYPOGRAPHY
            ------------------------------------- */
            h1,
            h2,
            h3,
            h4 {
                color: #000000;
                font-family: sans-serif;
                font-weight: 400;
                line-height: 1.4;
                margin: 0;
                Margin-bottom: 30px; }

            h1 {
                font-size: 35px;
                font-weight: 300;
                text-align: center;
                text-transform: capitalize; }

            p,
            ul,
            ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                Margin-bottom: 15px; }
            p li,
            ul li,
            ol li {
                list-style-position: inside;
                margin-left: 5px; }

            a {
                color: #3498db;
                text-decoration: underline; }

            /* -------------------------------------
                BUTTONS
            ------------------------------------- */
            .btn {
                box-sizing: border-box;
                width: 100%;
                background-color: #ffffff;
                border: solid 1px #3498db;
                border-radius: 5px;
                box-sizing: border-box;
                color: #3498db;
                cursor: pointer;
                display: inline-block;
                font-size: 14px;
                font-weight: bold;
                margin: 0;
                padding: 12px 25px;
                text-decoration: none;
                text-transform: capitalize;
            }
            .btn-primary{
                background-color: #3498db;
                border-color: #3498db;
                color: #ffffff;
            }

            /* -------------------------------------
                OTHER STYLES THAT MIGHT BE USEFUL
            ------------------------------------- */
            .last {
                margin-bottom: 0; }

            .first {
                margin-top: 0; }

            .align-center {
                text-align: center; }

            .align-right {
                text-align: right; }

            .align-left {
                text-align: left; }

            .clear {
                clear: both; }

            .mt0 {
                margin-top: 0; }

            .mb0 {
                margin-bottom: 0; }

            .preheader {
                color: transparent;
                display: none;
                height: 0;
                max-height: 0;
                max-width: 0;
                opacity: 0;
                overflow: hidden;
                mso-hide: all;
                visibility: hidden;
                width: 0; }

            .powered-by a {
                text-decoration: none; }

            hr {
                border: 0;
                border-bottom: 1px solid #f6f6f6;
                Margin: 20px 0; }

            /* -------------------------------------
                RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important; }
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important; }
                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important; }
                table[class=body] .content {
                    padding: 0 !important; }
                table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important; }
                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important; }
                table[class=body] .btn table {
                    width: 100% !important; }
                table[class=body] .btn a {
                    width: 100% !important; }
                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important; }}

            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
                .ExternalClass {
                    width: 100%; }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%; }
                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important; }
                .btn-primary table td:hover {
                    background-color: #34495e !important; }
                .btn-primary a:hover {
                    background-color: #34495e !important;
                    border-color: #34495e !important; } }


            h1,h2,h3,h4,h5,h6{font-family:inherit;font-weight:500;line-height:1.1;color:inherit}
            h1{
                font-size: 36px;
            }
            h2{
                font-size: 32px;
            }
            h3{
                font-size: 28px;
            }
            h4{
                font-size: 22px;
            }
            h5{
                font-size: 18px;
            }
            h6{
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" class="body">
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td class="container">
                        <div class="content">
                            <table class="main">
                                <!-- START MAIN CONTENT AREA -->
                                <tbody>
                                    <tr>
                                        <td class="wrapper">
                                            <table class="main">
                                                <tbody>
                                                    <tr>
                                                        <td class="wrapper">
                                                            @php $options=\App\Models\Option::pluck('title','id') @endphp
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <h3>{{trans('email.Agent registeration')}}</h3>

                                                                            <h4>{{trans('email.Agent information')}}</h4>
                                                                            <p>                                 
                                                                                <strong>{{trans("email.Name")}} : </strong> {{$row->name}}
                                                                            </p>

                                                                            <h4>{{trans('email.Company details')}}</h4>
                                                                            <p>
                                                                                <strong>{{trans("email.Company Name")}} : </strong> {{$row->company_name}} </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Company registeration number")}} : </strong> {{$row->company_registeration_number}} </p>
                                                                            <p>  
                                                                                <strong>{{trans("email.IATA status")}} : </strong> {{@$options[$row->iata_status]}}</p>
                                                                            <p>
                                                                                <strong>{{trans("email.IATA Number")}} : </strong> {{$row->iata_number}} </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Nature of business")}} : </strong> {{$row->nature_of_business_id}} </p>
                                                                            <p> 
                                                                                <strong>{{trans("email.Website")}} : </strong> {{$row->website}} </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Address")}} : </strong> {{$row->address}} </p>
                                                                            <p> 
                                                                                <strong>{{trans("email.Country")}} : </strong> {{@$row->country->title}} </p>
                                                                            <p> 
                                                                                <strong>{{trans("email.City")}} : </strong> {{@$row->city->title}} </p>
                                                                            <p>  
                                                                                <strong>{{trans("email.Pin Code")}} : </strong> {{$row->pin_code}} </p>
                                                                            <p>  


                                                                            <h4>{{trans('email.Customer information')}}</h4>
                                                                            <p>
                                                                                <strong>{{trans("email.First name")}} : </strong> {{$row->first_name}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Last name")}} : </strong> {{$row->last_name}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Designation")}} : </strong> {{$row->designation}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Email")}} : </strong> {{$row->email}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Phone")}} : </strong> {{$row->phone}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Mobile")}} : </strong> {{$row->mobile}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Fax")}} : </strong> {{$row->fax}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Currency")}} : </strong> {{$row->currency_id}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Time Zone")}} : </strong> {{@$options[$row->timezone]}}
                                                                            </p>

                                                                            <h4>{{trans('email.Finance account')}}</h4>
                                                                            <h5>{{trans('email.Accounts')}}</h5>
                                                                            <p>
                                                                                <strong>{{trans("email.Account name")}} : </strong> {{$row->account_name}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Account email")}} : </strong> {{$row->account_email}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Account contact number")}} : </strong> {{$row->account_contact_number}}
                                                                            </p>

                                                                            <h5>{{trans('email.Reservation')}}</h5>
                                                                            <p>
                                                                                <strong>{{trans("email.Reservation name")}} : </strong> {{$row->reservation_name}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Reservation email")}} : </strong> {{$row->reservation_email}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Reservation contact number")}} : </strong> {{$row->reservation_contact_number}}
                                                                            </p>

                                                                            <h5>{{trans('email.Managment')}}</h5>
                                                                            <p>
                                                                                <strong>{{trans("email.Managment name")}} : </strong> {{$row->management_name}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Reservation email")}} : </strong> {{$row->management_email}}
                                                                            </p>
                                                                            <p>
                                                                                <strong>{{trans("email.Reservation contact number")}} : </strong> {{$row->management_contact_number}}
                                                                            </p>
                                                                            @if($row->logo)
    <p>
        <strong>{{trans("admin.Logo")}} : </strong> 
        <a href="{{App::make("url")->to('/')}}/uploads/large/{{$row->logo}}">
            <img src="{{App::make("url")->to('/')}}/uploads/small/{{$row->logo}}" width="100">
        </a>
    </p>
    @endif
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- END MAIN CONTENT AREA -->
                                                </tbody>
                                            </table>

                                            <!-- START FOOTER -->
                                            <div class="footer">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="content-block powered-by">
                                                                {{trans("email.Copyright")}} {{date("Y")}} © <a href="{{App::make("url")->to('/')}}">{{$configs['site_title'] or env('SITE_TITLE')}}</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>