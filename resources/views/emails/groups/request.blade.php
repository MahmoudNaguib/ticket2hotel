<html><head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>{{trans("email.New Group Request")}}</title>
        <style>
            /* -------------------------------------
                GLOBAL RESETS
            ------------------------------------- */
            img {
                border: none;
                -ms-interpolation-mode: bicubic;
                max-width: 100%; }

            body {
                background-color: #f6f6f6;
                font-family: sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%; }

            table {
                border-collapse: separate;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                width: 100%; }
            table td {
                font-family: sans-serif;
                font-size: 14px;
                vertical-align: top; }

            /* -------------------------------------
                BODY & CONTAINER
            ------------------------------------- */

            .body {
                background-color: #f6f6f6;
                width: 100%; }

            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
                display: block;
                Margin: 0 auto !important;
                /* makes it centered */
/*                max-width: 580px;*/
                padding: 10px;
                width: auto !important;
/*                width: 580px; */
            }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                box-sizing: border-box;
                display: block;
                Margin: 0 auto;
/*                max-width: 580px;*/
                padding: 10px;
            }

            /* -------------------------------------
                HEADER, FOOTER, MAIN
            ------------------------------------- */
            .main {
                background: #fff;
                border-radius: 3px;
                width: 100%; }

            .wrapper {
                box-sizing: border-box;
                padding: 20px; }

            .footer {
                clear: both;
                padding-top: 10px;
                text-align: center;
                width: 100%; }
            .footer td,
            .footer p,
            .footer span,
            .footer a {
                color: #999999;
                font-size: 12px;
                text-align: center; }

            /* -------------------------------------
                TYPOGRAPHY
            ------------------------------------- */
            h1,
            h2,
            h3,
            h4 {
                color: #000000;
                font-family: sans-serif;
                font-weight: 400;
                line-height: 1.4;
                margin: 0;
                Margin-bottom: 30px; }

            h1 {
                font-size: 35px;
                font-weight: 300;
                text-align: center;
                text-transform: capitalize; }

            p,
            ul,
            ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                Margin-bottom: 15px; }
            p li,
            ul li,
            ol li {
                list-style-position: inside;
                margin-left: 5px; }

            a {
                color: #3498db;
                text-decoration: underline; }

            /* -------------------------------------
                BUTTONS
            ------------------------------------- */
            .btn {
                box-sizing: border-box;
                width: 100%;
                background-color: #ffffff;
                border: solid 1px #3498db;
                border-radius: 5px;
                box-sizing: border-box;
                color: #3498db;
                cursor: pointer;
                display: inline-block;
                font-size: 14px;
                font-weight: bold;
                margin: 0;
                padding: 12px 25px;
                text-decoration: none;
                text-transform: capitalize;
            }
            .btn-primary{
                background-color: #3498db;
                border-color: #3498db;
                color: #ffffff;
            }

            /* -------------------------------------
                OTHER STYLES THAT MIGHT BE USEFUL
            ------------------------------------- */
            .last {
                margin-bottom: 0; }

            .first {
                margin-top: 0; }

            .align-center {
                text-align: center; }

            .align-right {
                text-align: right; }

            .align-left {
                text-align: left; }

            .clear {
                clear: both; }

            .mt0 {
                margin-top: 0; }

            .mb0 {
                margin-bottom: 0; }

            .preheader {
                color: transparent;
                display: none;
                height: 0;
                max-height: 0;
                max-width: 0;
                opacity: 0;
                overflow: hidden;
                mso-hide: all;
                visibility: hidden;
                width: 0; }

            .powered-by a {
                text-decoration: none; }

            hr {
                border: 0;
                border-bottom: 1px solid #f6f6f6;
                Margin: 20px 0; }

            /* -------------------------------------
                RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important; }
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important; }
                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important; }
                table[class=body] .content {
                    padding: 0 !important; }
                table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important; }
                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important; }
                table[class=body] .btn table {
                    width: 100% !important; }
                table[class=body] .btn a {
                    width: 100% !important; }
                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important; }}

            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
                .ExternalClass {
                    width: 100%; }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%; }
                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important; }
                .btn-primary table td:hover {
                    background-color: #34495e !important; }
                .btn-primary a:hover {
                    background-color: #34495e !important;
                    border-color: #34495e !important; } }

        </style>
    </head>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" class="body">
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td class="container">
                        <div class="content">
                            <table class="main">
                                <!-- START MAIN CONTENT AREA -->
                                <tbody>
                                    <tr>
                                        <td class="wrapper">
                                            <table class="main">
                                                <tbody>
                                                    <tr>
                                                        <td class="wrapper">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <p>
                                                                                Dear Business partners,
                                                                            </p>
                                                                            <p>
                                                                                Here below received from tkt2htl client
                                                                            </p>

                                                                            <div style="border-style:ridge; padding: 15px;">



                                                                                <div class="table-responsive">
                                                                                    <h4><strong>{{trans("email.Group Information")}}</strong></h4>
                                                                                    @if(@$row->group_name)                                              
                                                                                    <p>
                                                                                        <label>
                                                                                            <strong>{{trans("email.Group name")}} </strong> 
                                                                                            {{$row->group_name}} 
                                                                                        </label>
                                                                                    </p>
                                                                                    @endif

                                                                                    @if(@$row->group_type)                                              
                                                                                    <p>
                                                                                        <label>
                                                                                            <strong>{{trans("email.Group type")}} </strong> 
                                                                                            {{$row->group_type}} 
                                                                                        </label>
                                                                                    </p>
                                                                                    @endif

                                                                                    @if(@$row->nationality)                                              
                                                                                    <p>
                                                                                        <label>
                                                                                            <strong>{{trans("email.Nationality")}} </strong> 
                                                                                            {{$row->nationality}} 
                                                                                        </label>
                                                                                    </p>
                                                                                    @endif


                                                                                    @if(@$row->destination)                                              
                                                                                    <p>
                                                                                    <h4><strong>{{trans("email.Destination")}}</strong></h4> 
                                                                                    @php $destination=json_decode($row->destination); @endphp
                                                                                    @for($i=0; $i<sizeof(@$destination->country);$i++)
                                                                                        {{trans('email.Country')}} : {{@$destination->country[$i]}} <br>
                                                                                        {{trans('email.City')}} : {{@$destination->city[$i]}} <br>
                                                                                        {{trans('email.CheckIn')}} : {{@$destination->check_in[$i]}} <br>
                                                                                        {{trans('email.CheckOut')}} : {{@$destination->check_out[$i]}} <br>
                                                                                        ---------------------------------------- <br>
                                                                                        @endfor    
                                                                                        @endif

                                                                                        <h4><strong>{{trans("email.Types of Services")}}</strong></h4> 
                                                                                        @if(@$row->flight_checkbox) 
                                                                                        <h5><strong><u>{{trans('email.Flight')}}</u></strong></h5>
                                                                                        @if(@$row->flight)
                                                                                        @php $flight=json_decode($row->flight); @endphp
                                                                                        @foreach($flight as $k=>$v)
                                                                                        <p>
                                                                                            <strong>{{clean($k)}}</strong>
                                                                                            {{$v}}
                                                                                        </p>
                                                                                        @endforeach
                                                                                        @endif    
                                                                                        @endif

                                                                                        @if(@$row->hotel_checkbox) 
                                                                                        <h5><strong><u>{{trans('email.Hotel')}}</u></strong></h5>
                                                                                        @if(@$row->hotel)
                                                                                        @php $hotel=json_decode($row->hotel); @endphp
                                                                                        @foreach($hotel as $k=>$v)
                                                                                        <p>
                                                                                            <strong>{{clean($k)}}</strong>
                                                                                            {{$v}}
                                                                                        </p>
                                                                                        @endforeach
                                                                                        @endif    
                                                                                        @endif

                                                                                        @if(@$row->transfer_checkbox) 
                                                                                        <h5><strong><u>{{trans('email.Transfer')}}</u></strong></h5>
                                                                                        @if(@$row->transfer)
                                                                                        @php $transfer=json_decode($row->transfer); @endphp
                                                                                        @foreach($transfer as $k=>$v)
                                                                                        <p>
                                                                                            <strong>{{clean($k)}}</strong>
                                                                                            {{$v}}
                                                                                        </p>
                                                                                        @endforeach
                                                                                        @endif    
                                                                                        @endif

                                                                                        @if(@$row->tours_checkbox) 
                                                                                        <h5><strong><u>{{trans('email.Tours')}}</u></strong></h5>
                                                                                        @if(@$row->tours)
                                                                                        @php $tours=json_decode($row->tours); @endphp
                                                                                        @foreach($tours as $k=>$v)
                                                                                        <p>
                                                                                            <strong>{{clean($k)}}</strong>
                                                                                            {{$v}}
                                                                                        </p>
                                                                                        @endforeach
                                                                                        @endif    
                                                                                        @endif


                                                                                        @if(@$row->others_checkbox) 
                                                                                        <h5><strong><u>{{trans('email.Others')}}</u></strong></h5>
                                                                                        @if(@$row->others)
                                                                                        @php $others=json_decode($row->others); @endphp
                                                                                        @foreach($others as $k=>$v)
                                                                                        <p>
                                                                                            <strong>{{clean($k)}}</strong>
                                                                                            {{$v}}
                                                                                        </p>
                                                                                        @endforeach
                                                                                        @endif    
                                                                                        @endif

                                                                                        <h4><strong>{{trans("email.Contact information")}}</strong></h4>
                                                                                        @if(@$row->company_name)                                                                            
                                                                                        <p><label><strong>{{trans("email.Company name")}} </strong> {{$row->company_name}} </label></p>
                                                                                        @endif 
                                                                                        @if(@$row->contact_name)                                                                            
                                                                                        <p><label><strong>{{trans("email.Contact name")}} </strong> {{$row->contact_name}} </label></p>
                                                                                        @endif 

                                                                                        @if(@$row->contact_mobile)                                                                            
                                                                                        <p><label><strong>{{trans("email.Contact mobile")}} </strong> {{$row->contact_mobile}} </label></p>
                                                                                        @endif

                                                                                        @if(@$row->contact_email)                                                                            
                                                                                        <p><label><strong>{{trans("email.Contact email")}} </strong> {{$row->contact_email}} </label></p>
                                                                                        @endif 
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <br>
<p>Looking forward your availability & question</p>

Best Regards,


<br><br><br>
Ops Tkt2htl
Contact Details<br>
Tel: 0224041350 <br>
Mobile: +201211000058


                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- END MAIN CONTENT AREA -->
                                                </tbody>
                                            </table>
                                            s</div>

                                            <!-- START FOOTER -->
                                            <div class="footer">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="content-block powered-by">
                                                                {{trans("email.Copyright")}} {{date("Y")}} © <a href="{{App::make("url")->to('/')}}">{{$configs['site_title'] or env('SITE_TITLE')}}</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>