@extends('front.layouts.master')

@section('content')
<!-- Intro -->
<div id="about">
    <div class="light-wrapper">
        <div class="container inner">
            <h2>{{trans('front.Unsubscribe from our newsletter')}}</h2>
            <p>{{trans('front.We will miss you')}}</p>
            <p>{{trans('front.Go Back click')}} <a href="{{App::make("url")->to('/')}}/">{{trans('front.here')}}</a></p>

        </div>
        <!-- /.container --> 
    </div>
    <!-- /.light-wrapper -->
</div>
<!-- /Intro -->s
@stop