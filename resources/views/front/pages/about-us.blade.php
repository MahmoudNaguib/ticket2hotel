@extends('front.layouts.master')
@section('content')
<div id="header">
    <div class="h1">
        <h1><span>{{ paragraph(1)->title }}</span>
    </div>
</div>
<main>
    <div class="centre">
        @include('front.partials.flash_messages')
        <div class="recent">
            <img src="uploads/large/{{ paragraph(1)->main_image }}">
        </div>
        <div class="footertestimonial">
            <i class="fa fa-quote-left"></i>
            <p class="title">{{ paragraph(1)->title }}</p>
            <p>
                {!! paragraph(1)->content !!}
            </p>
        </div>
        @php
        $partners=\App\Models\Partner::where('published',1)->get();
        @endphp
        @if($partners)
        <!-- Gallery Slider | START -->
        <div id="galleryslider" class="manual">
            <div class="slidecontainer">
                <div class="slider">
                    @foreach($partners as $partner)
                    <img alt="{{$partner->title}}" src="uploads/large/{{$partner->main_image}}" width="100" height="75" />
                    @endforeach
                </div>
                <div class="centre">
                    <div class="nav">
                        <a class="prev"><i class="fa fa-chevron-left"></i></a>
                        <a class="next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>
</main>
@stop


