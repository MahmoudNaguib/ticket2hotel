@extends('front.layouts.master')
@section('content')
@include('front.partials.header')
<main>
    <div class="centre">
        @include('front.partials.flash_messages')
        <h1><i class="fa fa-quote-left"></i> {{ paragraph(4)->title }}</h1>
        <p>{!! paragraph(4)->content !!}</p>
        <a href="about-us" class="button"><span data-hover="{{trans('front.About us')}}">{{trans('front.About us')}}</span></a>
    </div>
</main>
@stop


