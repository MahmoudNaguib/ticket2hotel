@extends('front.layouts.master')
@section('content')
<div id="header">
    <div class="h1">
        <h1><span>{{ paragraph(2)->title }}</span>
    </div>
</div>
<main>
    <div class="centre">
        @include('front.partials.flash_messages')
        <div class="recent">
            <img src="uploads/large/{{ paragraph(2)->main_image }}">
        </div>
        <div class="footertestimonial">
            <i class="fa fa-quote-left"></i>
            <p class="title">{{ paragraph(2)->title }}</p>
            <p>
                {!! paragraph(2)->content !!}
            </p>
        </div>
        <div class="row">
            <div class="col-md-6 mbs">
                <div class="row">
                    <div id="map" style="width:100%;height:450px;"></div>
                    <input type="hidden" name="latitude" value="{{$configs['latitude']}}" id="latitude">
                    <input type="hidden" name="latitude" value="{{$configs['longitude']}}" id="longitude">
                </div>
            </div>
            <div class="col-md-6">

                {!! Form::open(['url' => "contacts/send", 'method' => 'post','enctype'=>'multipart/form-data'] ) !!}
                @if (Session::has('message_sent'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                    {{ Session::get('message_sent') }}
                </div>
                @endif
                <div class="form-group">
                    @php $input='name'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control required','placeholder'=>trans("front.Name"),'required'=>'required']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="form-group">
                    @php $input='email'; @endphp
                    {!! Form::email($input,null,['class'=>'form-control required','placeholder'=>trans("front.Email"),'required'=>'required']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="form-group">
                    @php $input='mobile'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control required','placeholder'=>trans("front.Mobile"),'required'=>'required']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="form-group">
                    @php $input='subject'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control required','placeholder'=>trans("front.Subject"),'required'=>'required']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="form-group">
                    @php $input='message'; @endphp
                    {!! Form::textarea($input,null,['class'=>'form-control required','rows'=>'5','placeholder'=>trans("front.Message"),'required'=>'required']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>

                <button type="submit" class="form-control buttoner">{{trans('front.Submit')}}</button>
                <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</main>
@stop


@push('js')
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyA3tFgVYQ93q3PcrorMeGoGZX0jgIvCR7o"></script>
<script type="text/javascript">
$(function () {
    intialize();
});
function intialize() {
    var center_lat = $("#latitude").val();
    var center_lng = $("#longitude").val();
    //console.log(center_lat);
    var latlng = new google.maps.LatLng(center_lat, center_lng);
    var options = {
        zoom: 15,
        center: latlng,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
                /*--End style--*/
    };
    var map = new google.maps.Map(document.getElementById("map"), options);
    marker = new google.maps.Marker({
    position: new google.maps.LatLng(center_lat, center_lng),
            map: map
            //icon: '{{App::make("url")->to('/')}}/assets/images/pin.png'
    });
            var content = '<div class="map_content">' + '<h5>{{@$configs['site_title']}}</h5><p>{{@$configs['address']}}</p>' + '</div>';
            var infowindow = new google.maps.InfoWindow({
                content: content
            });
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
}
$(window).resize(function () {
    intialize();
});
</script>
@endpush

