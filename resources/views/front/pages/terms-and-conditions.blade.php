@extends('front.layouts.master')
@section('content')
<div id="header">
    <div class="h1">
        <h1><span>{{ paragraph(3)->title }}</span>
    </div>
</div>
<main>
    <div class="centre">
        @include('front.partials.flash_messages')
        <h1><i class="fa fa-quote-left"></i> {{ paragraph(3)->title }}</h1>
        <p>
            {!! paragraph(3)->content !!}
        </p>
    </div>
</main>
@stop


