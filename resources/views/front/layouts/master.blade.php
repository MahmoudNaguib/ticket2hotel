<!DOCTYPE html>
<!--[if IE 7]> <html class="ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> 
<!--<![endif]-->
<html lang="en">
    <head>
        @include('front.partials.meta')
        @include('front.partials.css')
        @stack('css')
    </head>
    <body>
        @include('front.partials.navigation')
        <div id="container">
            <!--main content start-->
            @yield('content')
            <!--main content start-->
            @include('front.partials.footer')
        </div>
        @include('front.partials.js')
        @stack('js')
    </body>
</html>
