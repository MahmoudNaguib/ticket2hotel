@extends('front.layouts.master')

@section('content')
<div class="container">
    {!! Form::open(['url' => 'auth/login', 'method' => 'post','class'=>'form-horizontal','enctype'=>'multipart/form-data'] ) !!}
    <div class="login-wrap">
        @php $input='email'; @endphp
        <div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
            {!! Form::label($input,trans('front.Email'),['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">
                {!! Form::email($input,null,['class'=>'form-control','required'=>'required']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
        </div>
        @php $input='password'; @endphp
        <div class="form-group {{ $errors->has($input) ? 'has-error' : '' }}">
            {!! Form::label($input,trans('front.Password'),['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">
                {!! Form::password($input,['class'=>'form-control','required'=>'required']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
        </div>
        <hr>
        <button type="submit" class="btn btn-primary"><i class="fa fa-lock"></i> {{trans('front.Login')}}</button>
        <button type="reset" class="btn btn-default">{{trans('front.Reset')}}</button>
        <hr>
        <a href="auth/forgot-password">{{trans('front.Forgot password')}}</a> | <a href="auth/register">{{trans('front.Register')}}</a>
    </div>
</form>
</div>
@stop
