@extends('front.layouts.master')
@section('content')
<div id="header">
    <div class="h1">
        <h1><span>{{$page_title}}</span>
    </div>
</div>
<main>
    <div class="centre">
        <h1><i class="fa fa-quote-left"></i> {{$page_title}}</h1>
        <p>
        <div id="contact">
            @include('front.partials.flash_messages')
            {!! Form::model($row, ['url' => 'register', 'method' => 'post','class'=>'','enctype'=>'multipart/form-data'] ) !!}
            <h3>{{trans('front.Agent registeration')}}</h3>
            <div class="field">
                @php $input='name'; @endphp
                {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Name').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <br>
            <h3>{{trans('front.Company details')}}</h3>
            <div class="col">
                <div class="field">
                    @php $input='company_name'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Company name').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='iata_number'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Iata number').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='address'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Address').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='pin_code'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Pin Code').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <div class="field">
                    @php $input='company_registeration_number'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Company registeration number').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='nature_of_business_id'; @endphp
                    {!! Form::select($input,\App\Models\Option::where('type','nature_of_business')->published()->pluck('title','id'), null, ['class' => 'form-control','placeholder'=>trans('front.Nature of business').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='country_id'; @endphp
                    {!! Form::select($input,\App\Models\Country::published()->pluck('title','id'), null, ['class' => 'form-control','placeholder'=>trans('front.Country'),'id'=>'country_id']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <div class="field">
                    @php $input='iata_status'; @endphp
                    {!! Form::select($input,['0'=>trans('front.Not approved'),'1'=>trans('front.Approved')], null, ['class' => 'form-control','placeholder'=>trans('front.IATA status').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='website'; @endphp
                    {!! Form::url($input,null,['class'=>'form-control','placeholder'=>trans('front.Website').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    <select name="city_id" id="city_id">
                        <option selected="selected" disabled="disabled" hidden="hidden" value="">{{trans('front.City')}}</option>
                        @foreach(\App\Models\City::published()->get() as $city)
                        <option value="{{$city->id}}" class="country_{{$city->country_id}}">{{$city->title}}</option>
                        @endforeach
                    </select>
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
            </div>
            <div class="clear"></div>
            <h3>{{trans('front.Customer information')}}</h3>
            <div class="col">
                <div class="field">
                    @php $input='first_name'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.First name').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='email'; @endphp
                    {!! Form::email($input,null,['class'=>'form-control','placeholder'=>trans('front.Email').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='fax'; @endphp
                    {!! Form::email($input,null,['class'=>'form-control','placeholder'=>trans('front.Fax').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <div class="field">
                    @php $input='last_name'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Last name').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='phone'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Phone').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='currency_id'; @endphp
                    {!! Form::select($input,\App\Models\Option::where('type','currency')->published()->pluck('title','id'), null, ['class' => 'form-control','placeholder'=>trans('front.Currency').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <div class="field">
                    @php $input='designation'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Designation').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='mobile'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Mobile').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='timezone'; @endphp
                    {!! Form::select($input,\App\Models\Option::where('type','timezone')->published()->pluck('title','id'), null, ['class' => 'form-control','placeholder'=>trans('front.Timezone').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
            </div>

            <div class="clear"></div>
            <h3>{{trans('front.Finance account')}}</h3>
            <div class="col">
                <h4>{{trans('front.Accounts')}}</h4>
                <div class="field">
                    @php $input='account_name'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Account name').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='account_email'; @endphp
                    {!! Form::email($input,null,['class'=>'form-control','placeholder'=>trans('front.Account email').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='account_contact_number'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Account contact number').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='logo'; @endphp
                     {!! Form::file($input,['class'=>'form-control fileinput','accept'=>'image/*','data-show-preview'=>'false','data-allowed-file-extensions'=>'["jpg", "png","jpeg"]']) !!}
                </div>
            </div>
            <div class="col">
                <h4>{{trans('front.Reservations/Operations')}}</h4>
                <div class="field">
                    @php $input='reservation_name'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Reservation name').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='reservation_email'; @endphp
                    {!! Form::email($input,null,['class'=>'form-control','placeholder'=>trans('front.Reservation email').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='reservation_contact_number'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Reservation contact number').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <h4>{{trans('front.Management')}}</h4>
                <div class="field">
                    @php $input='management_name'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Management name').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='management_email'; @endphp
                    {!! Form::email($input,null,['class'=>'form-control','placeholder'=>trans('front.Management email').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                <div class="field">
                    @php $input='management_contact_number'; @endphp
                    {!! Form::text($input,null,['class'=>'form-control','placeholder'=>trans('front.Management contact number').' (*)']) !!}
                    @foreach($errors->get($input) as $message)
                    <span class = 'help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                </div>
                
            </div>
            <div class="clear"></div>
            <label>
                <input name="terms_conditions" type="radio" value="1" style="width: auto;">
                {{trans('front.I Agree to')}} <a href="terms-and-conditions" style="position: relative;" target="_blank">{{trans('front.The Terms & Conditions')}}.</a>
            </label>
            @foreach($errors->get('terms_conditions') as $message)
            <span class = 'help-inline text-danger'>{{ $message }}</span>
            @endforeach
            <div class="clear"></div>
            <button><span data-hover="{{trans('front.Register')}}">{{trans('front.Register')}}</span></button>
            {!! Form::close() !!}
        </div>
        </p>
    </div>
</main>
@stop


@push('js')
<script>
    $(function () {
        $("#city_id option").hide();
        $('#country_id').on('change', function () {
            $("#city_id option").hide();
            $('.country_' + $(this).val()).show();
        });
    });
</script>
@endpush