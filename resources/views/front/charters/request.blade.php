@extends('front.layouts.master')
@section('content')
<div id="header">
    <div class="h1">
        <h1><span>{{$page_title}}</span>
    </div>
</div>
<main>
    <div class="centre">
        <h1><i class="fa fa-quote-left"></i> {{$page_title}}</h1>
        <p>
        <div id="contact">
            @include('front.partials.flash_messages')
            {!! Form::model($row, ['url' => 'charters/request', 'method' => 'post','class'=>'','enctype'=>'multipart/form-data'] ) !!}
            <h3>{{trans('front.Request a charter')}}</h3>
            <div class="field">
                @php 
                $input='type'; 
                $options=\App\Models\Charter::types();
                @endphp
                {!! Form::select($input,$options, null, ['class' => 'form-control','placeholder'=>trans('front.Type').' (*)','id'=>'type']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field all_fields" style="display: none;" id="type_1">
                @php $input='no_of_passengers'; @endphp
                {!! Form::number($input,null,['id'=>'no_of_passengers','class'=>'form-control','placeholder'=>trans('front.Number of passengers').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field all_fields" style="display: none;" id="type_2">
                @php $input='total_weight'; @endphp
                {!! Form::number($input,null,['id'=>'total_weight','class'=>'form-control','placeholder'=>trans('front.Total weight in KG').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php $input='from'; @endphp
                <!--                {!! Form::select($input,$options, null, ['id'=>'from','class' => 'form-control chosen','placeholder'=>trans('front.From').' (*)']) !!}-->
                {!! Form::text($input,null,['id'=>'from','class'=>'form-control','placeholder'=>trans('front.From').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php $input='to'; @endphp
                <!--                {!! Form::select($input,$options, null, ['id'=>'to','class' => 'form-control chosen','placeholder'=>trans('front.To').' (*)']) !!}-->
                {!! Form::text($input,null,['id'=>'to','class'=>'form-control','placeholder'=>trans('front.To').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>

            <div class="field">
                @php $input='date_of_operation'; @endphp
                {!! Form::text($input,null,['id'=>'date_of_operation','class'=>'form-control','placeholder'=>trans('front.Date of operation').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php 
                $input='prefered_etd';
                $options=\App\Models\Charter::etdOptions();
                @endphp
                {!! Form::select($input,$options, null, ['class' => 'form-control','placeholder'=>trans('front.Preferred ETD').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php $input='notes'; @endphp
                {!! Form::textarea($input,null,['class'=>'form-control','placeholder'=>trans('front.Notes').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>


            <div class="clear"></div>
            <h3>{{trans('front.Contact Information')}}</h3>
            <div class="field">
                @php $input='contact_email'; @endphp
                {!! Form::email($input,null,['id'=>'tp','class'=>'form-control','placeholder'=>trans('front.Contact email').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php $input='contact_mobile'; @endphp
                {!! Form::text($input,null,['id'=>'tp','class'=>'form-control','placeholder'=>trans('front.Contact mobile').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php $input='contact_name'; @endphp
                {!! Form::text($input,null,['id'=>'tp','class'=>'form-control','placeholder'=>trans('front.Contact name').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="clear"></div>
            <button><span data-hover="{{trans('front.Submit')}}">{{trans('front.Submit')}}</span></button>

        </div>

        {!! Form::close() !!}
    </div>
</p>
</div>
</main>
@stop

@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .ui-datepicker-month, .ui-datepicker-year{
        color:black !important;
    }

</style>
@endpush

@push('js')
<script src="assets/js/jqueryui.js"></script>
<script src="assets/js/airports.js?id=4"></script>
<script>
$(".chosen").chosen();
function resetAll() {
    $('.all_fields #total_weight').val('');
    $('.all_fields #no_of_passengers').val('');
}
$(function () {
    resetAll();
    $('#type').on('change', function () {
        resetAll();
        $('.all_fields').hide();
        if ($(this).val() == 3) {
            $('#type_1').show();
            $('#type_2').show();
        } else {
            $('#type_' + $(this).val()).show();
        }
    });
    $('#type').trigger('change');
    $("#from").autocomplete({source: airports, minLength: 2});
    $("#from").on("autocompleteselect", function (e, ui) {
        e.preventDefault(); // prevent the "value" being written back after we've done our own changes
        this.value = ui.item.label;
        // Put ui.item.value somewhere
    });
    $("#to").autocomplete({source: airports, minLength: 2});
    $("#to").on("autocompleteselect", function (e, ui) {
        e.preventDefault(); // prevent the "value" being written back after we've done our own changes
        this.value = ui.item.label;
        // Put ui.item.value somewhere
    });
    $("#date_of_operation").datepicker();
});
</script>
@endpush