@extends('front.layouts.master')
@section('content')
<div id="header">
    <div class="h1">
        <h1><span>{{$page_title}}</span>
    </div>
</div>
<main>
    <div class="centre">
        <h1><i class="fa fa-quote-left"></i> {{$page_title}}</h1>
        <p>
        <div id="contact">
            @include('front.partials.flash_messages')
            {!! Form::model($row, ['url' => 'groups/request', 'method' => 'post','class'=>'','enctype'=>'multipart/form-data'] ) !!}
            <h3>{{trans('front.Group information')}}</h3>
            <div class="field">
                @php $input='group_name'; @endphp
                {!! Form::text($input,null,['id'=>'group_name','class'=>'','placeholder'=>trans('front.Group name').' (*)','required'=>'required']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php $group_types=['Leisure'=>'Leisure','Incentive'=>'Incentive','Corporate'=>'Corporate','Other'=>'Other'] @endphp
                {!! Form::select("group_type",$group_types, null, ['class' => '','placeholder'=>trans('front.Group Type').' (*)','required'=>'required']) !!}
            </div>
            
            <div class="field">
                @php $countries=['0'=>trans('front.Not Specified')]+\App\Models\Country::published()->pluck('title','title')->toArray(); @endphp
                {!! Form::select("nationality",$countries, null, ['class' => '','placeholder'=>trans('front.Nationality')]) !!}
            </div>
            <div class="clear"></div>
            <h3>{{trans('front.Destination')}}</h3>
            <div class="destination">
                <div class="destination_fields">
                    <div class="field">
                        @php $countries=\App\Models\Country::published()->pluck('title','title'); @endphp
                        {!! Form::select("destination[country][]",$countries, null, ['class' => 'country','placeholder'=>trans('front.Country').' (*)','required'=>'required']) !!}
                        
                        
                        @php $cities=\App\Models\Country::published()->get(); @endphp
                            <option selected="selected" disabled="disabled" hidden="hidden" value="">{{trans('front.Choose city')}}</option>
                            @foreach($cities as $city)
                            <option value="{{$city->title}}" style="display: none;" class="country_{{$city->country_id}}">{{$city->title}}</option>
                            @endforeach
                        
                    </div>
                    <div class="field">
                        <select name="destination[city]" class="cityChild">
                            @php $cities=\App\Models\City::published()->get(); @endphp
                            <option selected="selected" disabled="disabled" hidden="hidden" value="">{{trans('front.Choose city')}}</option>
                            @foreach($cities as $city)
                            <option value="{{$city->title}}" style="display: none;" class="country_{{$city->country_id}}">{{$city->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="field">
                        <label><strong>{{trans('front.Check In')}}</strong></label>
                        {!! Form::text("destination[check_in][]",null,['class'=>' datepicker','placeholder'=>trans('front.Check in').' (*)','required'=>'required']) !!}
                    </div>
                    <div class="field">
                        <label><strong>{{trans('front.Check Out')}}</strong></label>
                        {!! Form::text("destination[check_out][]",null,['class'=>' datepicker','placeholder'=>trans('front.Check out').' (*)','required'=>'required']) !!}
                    </div>
                </div>
            </div> 

            <div style="text-align: right; position: relative;">
                <a href="groups/request#" class="add_more add_destination">+{{trans('front.Add More')}}</a>
            </div>
            <div class="clear"></div>
            <h3>{{trans('front.Types of services')}}</h3>
            <div class="field">
                <div>
                    {!! Form::checkbox('flight_checkbox',1, NULL,['id'=>'flight_checkbox']) !!} 
                    <label for="flight_checkbox">
                        {{trans('front.Flight')}}
                    </label>
                    <div class="flight_checkbox_fields" style="display: none;">
                        @php $options=['Economy class'=>'Economy class','Business class'=>'Business class','First class'=>'First class'] @endphp
                        {!! Form::select("flight[class]",$options, null, ['class' => 's','placeholder'=>trans('front.Choose Class').' (*)','required'=>'required']) !!}
                        {!! Form::text("flight[preferred_airline_name]",null,['class'=>'','placeholder'=>trans('front.Preferred airline name')]) !!}
                    </div>
                </div>
                <div>
                    {!! Form::checkbox('hotel_checkbox',1, NULL,['id'=>'flight_checkbox']) !!} 
                    <label for="hotel_checkbox">
                        {{trans('front.Hotel')}}
                    </label>
                    <div class="hotel_checkbox_fields" style="display: none;">
                        @php $options=['Hotel apartment'=>'Hotel apartment','VILLAS'=>'VILLAS','HOSTEL'=>'HOSTEL'] @endphp
                        {!! Form::select("hotel[hotel_type]",$options, null, ['class' => 'small_width','placeholder'=>trans('front.Hotel Type')]) !!}
                        
                        @php $options=['1'=>'1*','2'=>'2*','3'=>'3*','4'=>'4*','5'=>'5*'] @endphp
                        {!! Form::select("hotel[rate]",$options, null, ['class' => 'small_width','placeholder'=>trans('front.Star Rating')]) !!}
                        @php $options=['Rooms only'=>'Rooms only','Bed & Breakfast'=>'Bed & Breakfast','Half Board'=>'Half Board','Full board'=>'Full board','All Inclusive'=>'All Inclusive'] @endphp

                        {!! Form::select("hotel[meal_plan]",$options, null, ['class' => 'small_width','placeholder'=>trans('front.Meal Plan')]) !!}

                        <h4>{{trans('front.Number of rooms')}}</h4>
                        @php $options=range(0,30) @endphp
                        {!! Form::number("hotel[number_of_rooms_single]",null,['class'=>'small_width','placeholder'=>trans('front.Single'),'min'=>0]) !!}
                        {!! Form::number("hotel[number_of_rooms_double]",null,['class'=>'small_width','placeholder'=>trans('front.Double'),'min'=>0]) !!}
                        {!! Form::number("hotel[number_of_rooms_triple]",null,['class'=>'small_width','placeholder'=>trans('front.Triple'),'min'=>0]) !!}
                        
                        {!! Form::number("hotel[number_of_rooms_quad]",null,['class'=>'small_width','placeholder'=>trans('front.Quad'),'min'=>0]) !!}
                        <br><br>
                        <div class="field">
                            {!! Form::text("hotel[others]",null,['class'=>'','placeholder'=>trans('front.Others')]) !!}
                        </div>
                        <h4>{{trans('front.Number of guests')}}</h4>
                        {!! Form::number("hotel[number_of_adults]",null,['class'=>'small_width','placeholder'=>trans('front.Adults'),'min'=>0]) !!}
                        {!! Form::number("hotel[number_of_childs]",null,['class'=>'small_width','placeholder'=>trans('front.Childs'),'min'=>0]) !!}
                        {!! Form::number("hotel[number_of_infants]",null,['class'=>'small_width','placeholder'=>trans('front.Infants'),'min'=>0]) !!}
                        <br><br>
                        <div class="field">
                            {!! Form::text("hotel[preferred_hotel_name]",null,['class'=>'','placeholder'=>trans('front.Preferred Hotel Name')]) !!}
                        </div>
                    </div>
                </div>
                <div>
                    {!! Form::checkbox('transfer_checkbox',1, NULL,['id'=>'transfer_checkbox']) !!} 
                    <label for="transfer_checkbox">
                        {{trans('front.Transfer')}}
                    </label>
                    <div class="transfer_checkbox_fields" style="display: none;">
                        {!! Form::text("transfer[transfer]",null,['class'=>'','placeholder'=>trans('Transfer')]) !!}
                    </div>
                </div>
                <div>
                    {!! Form::checkbox('tours_checkbox',1, NULL,['id'=>'tours_checkbox']) !!} 
                    <label for="tours_checkbox">
                        {{trans('front.Tours')}}
                    </label>
                    <div class="tours_checkbox_fields" style="display: none;">
                        {!! Form::text("tours[tours]",null,['class'=>'','placeholder'=>trans('Tours')]) !!}
                    </div>
                </div>
                <div>
                    {!! Form::checkbox('others_checkbox',1, NULL,['id'=>'others_checkbox']) !!} 
                    <label for="others_checkbox">
                        {{trans('front.Others')}}
                    </label>
                    <div class="others_checkbox_fields" style="display: none;">
                        <div class="field">
                            {!! Form::text("others[others]",null,['class'=>'','placeholder'=>trans('front.Content').' (*)','required'=>'required']) !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <h3>{{trans('front.Contact Information')}}</h3>
            <div class="field">
                @php $input='company_name'; @endphp
                {!! Form::text($input,null,['id'=>'company_name','class'=>'','placeholder'=>trans('front.Company name').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            
            <div class="field">
                @php $input='contact_email'; @endphp
                {!! Form::email($input,null,['id'=>'contact_email','class'=>'','placeholder'=>trans('front.Contact email').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php $input='contact_mobile'; @endphp
                {!! Form::text($input,null,['id'=>'contact_mobile','class'=>'','placeholder'=>trans('front.Contact mobile').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            <div class="field">
                @php $input='contact_name'; @endphp
                {!! Form::text($input,null,['id'=>'contact_name','class'=>'','placeholder'=>trans('front.Contact name').' (*)']) !!}
                @foreach($errors->get($input) as $message)
                <span class = 'help-inline text-danger'>{{ $message }}</span>
                @endforeach
            </div>
            
            <div class="clear"></div>
            <button><span data-hover="{{trans('front.Submit')}}">{{trans('front.Submit')}}</span></button>
        </div>
        {!! Form::close() !!}
    </div>
</p>
</main>
@stop

@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
<style>
    .ui-datepicker-month, .ui-datepicker-year{
        color:black !important;
    }

</style>
@endpush


@push('js')
<script src="assets/js/jqueryui.js"></script>
<script src="assets/js/airports.js?id=4"></script>
<script>
$(function () {
    $('.country').on('change', function () {
            var childSelector=$(this).parent().parent().find('.cityChild');
            console.log(childSelector);
            childSelector.children('option').hide();
            console.log('.country_' + $(this).val());
            childSelector.children('.country_' + $(this).val()).show();
        });
    var destinationFields = $('.destination_fields').html();
    $(".datepicker").datepicker({changeMonth: true,changeYear: true, minDate: 0}).datepicker('setDate', 'today');
    $('.add_destination').on('click', function () {
        var random = Math.floor((Math.random() * 1000000000000) + 1);
        var deleteButton = '<div style="text-align: right; position: relative;"><a href="groups/request#" class="add_more delete_destination" data-item="item_' + random + '">+{{trans("front.Delete")}}</a></div><div class="clear"></div>';
        $('.destination').append('<div class="destination_fields" id="item_' + random + '">' + deleteButton + destinationFields + '</div>');
         $(".datepicker").datepicker({changeMonth: true,changeYear: true, minDate: 0}).datepicker('setDate', 'today');
        console.log('yes');
        $('.delete_destination').on('click', function () {
            console.log('delete' + $(this).data('item'));
            $("#" + $(this).data('item')).remove();
            return false;
        });
        return false;
    });
    $('input[name=flight_checkbox]').on('click', function () {
        if ($(this).is(":checked")) {
            console.log('checked');
            $('.flight_checkbox_fields select').val('');
            $('.flight_checkbox_fields').show();
        } else {
            console.log('unChecked');
            $('.flight_checkbox_fields select').val('');
            $('.flight_checkbox_fields').hide();
        }
    });
    $('input[name=hotel_checkbox]').on('click', function () {
        if ($(this).is(":checked")) {
            console.log('checked');
            $('.hotel_checkbox_fields select').val('');
            $('.hotel_checkbox_fields input').val('');
            $('.hotel_checkbox_fields').show();
        } else {
            console.log('unChecked');
            $('.hotel_checkbox_fields select').val('');
            $('.hotel_checkbox_fields input').val('');
            $('.hotel_checkbox_fields').hide();
        }
    });
    $('input[name=others_checkbox]').on('click', function () {
        if ($(this).is(":checked")) {
            console.log('checked');
            $('.others_checkbox_fields input').val('');
            $('.others_checkbox_fields').show();
        } else {
            console.log('unChecked');
            $('.others_checkbox_fields input').val('');
            $('.others_checkbox_fields').hide();
        }
    });
    
    $('input[name=transfer_checkbox]').on('click', function () {
        if ($(this).is(":checked")) {
            console.log('checked');
            $('.transfer_checkbox_fields input').val('');
            $('.transfer_checkbox_fields').show();
        } else {
            console.log('unChecked');
            $('.transfer_checkbox_fields input').val('');
            $('.transfer_checkbox_fields').hide();
        }
    });
    
    $('input[name=tours_checkbox]').on('click', function () {
        if ($(this).is(":checked")) {
            console.log('checked');
            $('.tours_checkbox_fields input').val('');
            $('.tours_checkbox_fields').show();
        } else {
            console.log('unChecked');
            $('.tours_checkbox_fields input').val('');
            $('.tours_checkbox_fields').hide();
        }
    });


});
</script>
@endpush