<div id="nav">
    <div class="centre">
        <a href="{{App::make("url")->to('/')}}/" class="logo">
            <img alt="{{@session('configs')['site_title']}}" src="uploads/{{@session('configs')['logo']}}" height="80"/>
        </a>
        <nav>
            <ul>
                <li><a href="{{App::make("url")->to('/')}}/">{{trans('front.Home')}}</a></li>
                <li><a href="about-us">{{trans('front.About us')}}</a></li>
                <li><a href="contact-us">{{trans('front.Contact us')}}</a></li>
<!--                <li><a href="terms-and-conditions">{{trans('front.Terms and conditions')}}</a></li>-->
                <li><a href="charters/request">{{trans('front.Request a charter')}}</a></li>
                <li><a href="groups/request">{{trans('front.Group request')}}</a></li>
            </ul>
            <a id="pull"><i class="fa fa-bars"></i></a>
        </nav>
        <!-- Languages | END -->
        <a href="register" class="book">
            <span data-hover="{{trans('front.Register now')}}">{{trans('front.Register now')}}</span> <i class="fa fa-check-circle"></i>
        </a>
        <div class="shadow"></div>
    </div>
</div>