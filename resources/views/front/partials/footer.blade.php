<footer>
    <div id="footer">
        <div class="centre">
            <!-- Subscribe / Social | START -->
            <div class="news">
                <div class="title"><span>{{trans('front.Subscribe')}}</span></div>
                <div class="subscribe">
                    <form action='subscribers/register' method="post" enctype="multipart/form-data">
                        @if (Session::has('subscription_succeeded'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('subscription_succeeded') }}
                        </div>
                        @endif
                        {!! Form::email('subscriber_email',null,['class'=>'form-control','placeholder'=>trans('front.Enter Your email'),'id'=>'subscribe-email','required'=>'required']) !!}
                        
                        <button><span data-hover="{{trans('front.Sign Up')}}">{{trans('front.Sign Up')}} </span></button>
                        @if(isset($errors))
                        <br>
                        @foreach($errors->get('subscriber_email') as $message)
                        <span class = 'help-inline text-danger'>{{ $message }}</span>
                        @endforeach
                        @endif
                    </form>
                </div>
                <div class="social">
                    @if(@session('configs')['facebook_link'])
                    <a href="{{@session('configs')['facebook_link']}}" target="_blank" title="{{trans('front.Facebook')}}"><i class="fa fa-facebook"></i></a>
                    @endif
                    @if(@session('configs')['twitter_link'])
                    <a href="{{@session('configs')['twitter_link']}}" target="_blank" title="{{trans('front.Twitter')}}"><i class="fa fa-twitter"></i></a>
                    @endif
                    @if(@session('configs')['instagram_link'])
                    <a href="{{@session('configs')['instagram_link']}}" target="_blank" title="{{trans('front.instagram')}}"><i class="fa fa-instagram"></i></a>
                    @endif
                </div>
            </div>
            <!-- Subscribe / Social | END -->
            <!-- Contact Details | START -->
            <div class="contact">
                <p><strong class="phone">{{trans('front.Telephone')}} : {{@session('configs')['telephone']}}</strong><br />
                    <a href="mailto:{{@session('configs')['info_email']}}">{{@session('configs')['info_email']}}</a><br />
                    <i class="fa fa-map-marker"></i> {{@session('configs')['address']}}<br /><br />
            </div>
            <!-- Contact Details | END -->
            <div class="dark"></div>
        </div>
    </div>
    <!-- Footer Links | START -->
    <div id="footerlinks">
        <div class="centre">
            <span>Copyright &copy; <script>var d = new Date(); document.write(d.getFullYear());</script> <strong>{{@session('configs')['site_title']}}</strong></span>
            <a href="{{App::make("url")->to('/')}}/">{{trans('front.Home')}}</a>
            <a href="about-us">{{trans('front.About us')}}</a>
            <a href="contact-us">{{trans('front.Contact us')}}</a>
            <a href="terms-and-conditions">{{trans('front.Terms and conditions')}}</a>
        </div>
    </div>
    <!-- Footer Links | END -->
</footer>