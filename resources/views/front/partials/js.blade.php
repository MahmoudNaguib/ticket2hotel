@php $configs=session('configs') @endphp
<script src="assets/js/plugins.js"></script>
<script src="assets/plugins/chosen/js/chosen.jquery.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/global.js?id=3"></script>

@if(@$configs['google_analytics_id'])
<script>
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

ga('create', '{{$configs['google_analytics_id']}}', 'auto');
ga('send', 'pageview');
</script>
@endif