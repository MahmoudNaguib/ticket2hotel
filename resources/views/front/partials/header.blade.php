<header>
    <!-- Featured Slider | START -->
    <div id="featured">
        @php
        $slides=\App\Models\Slide::where('published',1)->orderBy('order_field','asc')->get();
        @endphp
        @if($slides)
        <div class="slider">
            @foreach($slides as $slide)
            <div class="item">
                <div class="details">
                    @if($slide->show_title)
                    <div class="title" style="opacity: {{($slide->opacity)?:'0.5'}}">
                        <span>{{$slide->title}}</span>
                    </div>
                    @endif
                </div>
                <img alt="{{$slide->title}}" src="uploads/large/{{$slide->main_image}}" width="1800" height="800" />
            </div>
            @endforeach
        </div>
        <div class="centre">
            <div class="nav">
                <a class="prev"><i class="fa fa-chevron-left"></i></a>
                <a class="next"><i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
        @endif
    </div>
    <div id="check">
        <div class="col-md-offset-3 col-md-6">
            <div class="centre">
                <form action="{{session('configs')['reseller_link']}}" method="post">
                    <div class="field calendar">
                        <input name="resellerCode" type="text" placeholder="{{trans('front.Reseller code')}}" id="resellerCode"/>
                    </div>
                    <div class="field calendar">
                        <input name="username" type="text" placeholder="{{trans('front.Username')}}" id="username"/>
                    </div>
                    <button><span data-hover="{{trans('front.Login')}}">{{trans('front.Login')}}</span></button>

                </form>
            </div>
        </div>
    </div>
</header>