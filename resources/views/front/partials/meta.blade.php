<meta charset="utf-8">
<base href="{{App::make("url")->to('/')}}/" />
<title>{{$configs['site_title']}} - {{@$page_title}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Title here -->
<!-- Description, Keywords and Author -->
<meta name="description" content="{{$meta_description or @$configs['meta_description']}}">
<meta name="keywords" content="{{$meta_keywords or @$configs['meta_keywords']}}">
<meta name="author" content="{{@$configs['site_title']}}">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="language" content="English">
<!-- Social meta -->
<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<link rel="canonical" href="{{App::make("url")->to('/')}}{{Request::path()}}" />
<meta property="og:title" content="{{@$configs['site_title']}} - {{@$page_title}}"/>
<meta property="og:url" content="{{App::make("url")->to('/')}}{{Request::path()}}"/>
<meta property="og:site_name" content="{{@$configs['site_title']}}"/>
<meta property="og:description" content="{{$meta_description or $configs['meta_description']}}" />
@if(@$main_image)
@php $socialImage="uploads/large/".$main_image; @endphp
@else
@php $socialImage=App::make("url")->to('uploads/'.$configs['logo']); @endphp
@endif
@php list($width, $height) = @getimagesize($socialImage); @endphp
<meta property="og:image:width" content="{{@$width}}" />
<meta property="og:image:height" content="{{@$height}}" />
<meta property="og:image" content="{{$socialImage}}" />
<meta property="fb:app_id" content="966242223397117" />
<meta property="og:type" content="website" />

<!-- Favicon -->
<link rel="shortcut icon" href="uploads/{{$configs['favicon']}}">
<!--[if lt IE 9]>
        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->