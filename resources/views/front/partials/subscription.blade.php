<div class="container">
    <h3>{{trans('front.Subscribe to our newsletter')}}</h3>
    <form action='subscribers/register' method="post" enctype="multipart/form-data">
        @if (Session::has('subscription_succeeded'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('subscription_succeeded') }}
        </div>
        @endif
        <div class="form-group">
            {!! Form::email('subscriber_email',null,['class'=>'form-control','placeholder'=>trans('front.Enter Your email'),'id'=>'subscribe-email','required'=>'required']) !!}
            @if(isset($errors))
            @foreach($errors->get('subscriber_email') as $message)
            <span class = 'help-inline text-danger'>{{ $message }}</span>
            @endforeach
            @endif
        </div>
        <input name="subscribe" value="{{trans('front.Submit')}}" type="submit" class="form-control buttoner" />
    </form>
</div>