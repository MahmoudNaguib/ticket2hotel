<?php
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});

Route::prefix('admin')->group(function() {
    Route::get('/',
        function() {
        return redirect('admin/dashboard');
    });
    AdvancedRoute::controller('dashboard', 'Admin\Dashboard');
    AdvancedRoute::controller('auth', 'Admin\AdminAuthController');
    AdvancedRoute::controller('dashboard', 'Admin\Dashboard');
    AdvancedRoute::controller('configs', 'Admin\Configs');
    AdvancedRoute::controller('roles', 'Admin\Roles');
    AdvancedRoute::controller('admins', 'Admin\Admins');
    AdvancedRoute::controller('users', 'Admin\Users');
    AdvancedRoute::controller('paragraphs', 'Admin\Paragraphs');
    AdvancedRoute::controller('partners', 'Admin\Partners');
    AdvancedRoute::controller('contacts', 'Admin\Contacts');
    AdvancedRoute::controller('subscribers', 'Admin\Subscribers');
    AdvancedRoute::controller('options', 'Admin\Options');
    AdvancedRoute::controller('pages', 'Admin\Pages');
    AdvancedRoute::controller('slides', 'Admin\Slides');
    AdvancedRoute::controller('charters', 'Admin\Charters');
    AdvancedRoute::controller('groups', 'Admin\Groups');
});
AdvancedRoute::controller('auth', 'AuthController');
Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
AdvancedRoute::controller('contacts', 'Contacts');
AdvancedRoute::controller('pages', 'Pages');
AdvancedRoute::controller('subscribers', 'Subscribers');
AdvancedRoute::controller('register', 'Register');
AdvancedRoute::controller('charters', 'Charters');
AdvancedRoute::controller('groups', 'Groups');

Route::any('/terms-and-conditions', 'Pages@termsAndConditions');
Route::any('/contact-us', 'Pages@contactUs');
Route::any('/about-us', 'Pages@aboutUs');
Route::any('/', 'Pages@home');
