<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libs\Misc;
use Request;

class Group extends BaseModel {

    protected $guarded = [
    ];

    public static function boot() {
        parent::boot();
        static::deleted(function($row) {
            
        });
    }

    public static function types() {
        return [
            '1' => 'Passenger aircraft',
            '2' => 'All Cargo aircraft',
            '3' => 'Passenger and Caro aircraft'
        ];
    }
}