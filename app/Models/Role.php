<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends BaseModel {

    protected $guarded = [
        //  'slug',
    ];

    public function admins() {
        return $this->belongsToMany('App\Models\Admin', 'admin_role',
                'admin_id', 'role_id');
    }
}