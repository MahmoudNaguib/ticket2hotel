<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Request;

class Admin extends BaseModel {

    protected $table = "admins";
    //////
    protected $guarded = [
        'heades',
        'selectAll',
        'reset_token',
        'password_confirmation',
        'super_admin',
        'old_password',
        'deleted_at',
        'role_list'
    ];
    protected $hidden = ['password', 'reset_token', 'deleted_at'];

    public static function boot() {
        parent::boot();
        static::deleted(function($row) {
            $row->roles()->detach();
        });
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    public function roles() {
        return $this->BelongsToMany('App\Models\Role', 'admin_role', 'admin_id', 'role_id');
    }

    public function getRoleListAttribute() {
        return $this->roles->pluck('id')->toArray();
    }

}
