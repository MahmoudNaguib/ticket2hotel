<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Request;

class User extends BaseModel {

    protected $guarded = [
        'terms_conditions',
    ];
    protected $hidden = ['published', 'admin_id', 'updated_at'];
    public function country() {
        return $this->belongsTo('App\Models\Country');
    }
    public function city() {
        return $this->belongsTo('App\Models\City');
    }
}