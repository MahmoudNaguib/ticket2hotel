<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libs\Misc;
use Request;

class Option extends BaseModel {

    protected $guarded = [
    ];

    public function admin() {
        return $this->belongsTo('App\Models\Admin');
    }
    public static function types(){
        return [
            'nature_of_business'=>'Nature of business',
            'currency'=>'Currency',
            'timezone'=>'Timezone',
        ];
    }
}