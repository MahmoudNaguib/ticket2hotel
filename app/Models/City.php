<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends BaseModel {

    protected $guarded = [
    ];
    protected $table = 'cities';

    public function country() {
        return $this->belongsTo('App\Models\Country');
    }
}