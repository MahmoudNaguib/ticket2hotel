<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libs\Misc;
use Request;
use Session;
use DB;

class ViewComposerServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        $this->composeViews();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    private function composeViews() {
        $configs = Misc::get_configs();
        if (!Session::has('configs')) {
            $configs = Misc::get_configs();
            Session::put('configs', $configs);
        }
        ///////////////////////////////////////////////////// Admin
        view()->composer('admin.layouts.login', function($view) {
            $view->with('configs', Session::get('configs'));
        });
        view()->composer('admin.layouts.master', function($view) {
            $view->with('configs', Session::get('configs'));
        });
        /////////////////////////////////////////////////// end Admin
        //
        //
        //////////////////////////////////////////////////// Front
        view()->composer('front.layouts.index', function($view) {
            $view->with('configs', Session::get('configs'));
        });
        view()->composer('front.layouts.master', function($view) {
            $view->with('configs', Session::get('configs'));
        });
        view()->composer('front.pages.contact-us', function($view) {
            $view->with('configs', Session::get('configs'));
        });
        view()->composer('front.home.index', function($view) {
            $view->with('configs', Session::get('configs'));
        });
        view()->composer('front.pages.home', function($view) {
            $view->with('configs', Session::get('configs'));
        });
         view()->composer('front.pages.flight', function($view) {
            $view->with('configs', Session::get('configs'));
        });
        /////////////////////////////////////////////////// end Front
    }

}
