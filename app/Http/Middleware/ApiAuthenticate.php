<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class ApiAuthenticate {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $row = DB::table("users")->whereToken($request->input("token"))->wherePublished(1)->whereConfirmed(1)->first();
        if (!$row) {
            $row = DB::table("tokens")->whereToken($request->input("token"))->where("expiration_date",
                    ">", time())->first();
        }
        if (!$row) {
            $message = trans('Api.Unauthorized user.');
            return response()->json(['message' => $message], 401);
        }
        return $next($request);
    }
}