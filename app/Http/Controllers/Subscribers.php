<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front;
use Config;
use App;
use Auth;
use Session;
use Mail;
use Illuminate\Support\Facades\DB;

class Subscribers extends Front {

    public $model;
    public $module;
    public $rules;

     public function __construct(\App\Models\Subscriber $model) {
        parent::__construct();
        $this->module = 'subscribers';
        $this->model = $model;
    }

    public function anyRegister(Request $request) {
        $rules = [
            'subscriber_email' => 'required|email',
        ];
        $this->validate($request, $rules);
        $row = $this->model->whereEmail(trim($request->get('subscriber_email')))->first();
        if ($row) {
            $row->published = 1;
            $row->save();
        }
        else {
            $row = $this->model->create(['email' => trim($request->get('subscriber_email')),
                    'published' => 1]);
            $configs = Session::get('configs');
            try {
                Mail::send('emails.'.$this->module.'.welcome',
                    ['row' => $row, 'configs' => $configs],
                    function ($mail) use ($row, $configs) {
                    $mail->to($row->email)->subject(trans("front.Thanks for subscription at") . ' ' . env('SITE_TITLE'));
                });
            }
            catch (Exception $e) {
                // echo 'Caught exception: ', $e->getMessage(), "\n";
            }
        }
        $request->session()->flash('subscription_succeeded',
            trans('front.You have successfully subscribed to our newsletter'));
        return back();
    }

    public function getUnsubscribe($email) {
        $email = base64_decode($email);
        $row = $this->model->whereEmail($email)->first();
        if ($row) {
            $row->published = 0;
            $row->save();
        }
        flash()->success(trans('front.You have successfully unsubscribed to our newsletter'));
        return view('front.'.$this->module.'.unsubscribe');
    }
}