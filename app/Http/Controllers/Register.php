<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front;
use Config;
use App\Models\User;
use Session;
use Mail;
use Auth;

class Register extends Front {

    public $model;
    public $module;

    public function __construct(User $model) {
        parent::__construct();
        $this->module = 'users';
        $this->model = $model;
    }

    public function getIndex() {
        $row = \App\Models\Page::findOrFail(5);
        $page_title = $row->title;
        $meta_description = str_limit($row->meta_description, 190);
        $meta_keywords = str_limit($row->meta_keywords, 190);
        if ($row->main_image) {
            $main_image = $row->main_image;
        }
        $row = $this->model;
        return view('front.' . $this->module . '.register',
            compact('row', 'page_title', 'meta_description', 'meta_keywords',
                'main_image'));
    }

    public function postIndex(Request $request) {
        $rules = [
            'name' => 'required',
            'company_name' => 'required',
            'company_registeration_number' => 'required',
            'iata_status' => 'required',
            'iata_number' => 'required',
            'nature_of_business_id' => 'required',
            'website' => 'required',
            'address' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'pin_code' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'designation' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'mobile' => 'required',
            'fax' => 'required',
            'currency_id' => 'required',
            'timezone' => 'required',
            'account_name' => 'required',
            'reservation_name' => 'required',
            'management_name' => 'required',
            'account_email' => 'required|email',
            'reservation_email' => 'required|email',
            'management_email' => 'required|email',
            'account_contact_number' => 'required',
            'reservation_contact_number' => 'required',
            'management_contact_number' => 'required',
            'terms_conditions' => 'required'
        ];
        $this->validate($request, $rules);
        $row = $this->model->create($request->all());
        $imageSizes = ['small' => 'crop,100x100', 'large' => 'resize,1000x1000'];
        $this->model->uploadAndResize('logo', $row, $imageSizes);
        $configs = Session::get('configs');
        try {
            Mail::send('emails.users.register',
                ['row' => $row, 'configs' => $configs],
                function ($mail) use ($row, $configs) {
                $mail->to($configs['registeration_form_receive_email'],
                    env('SITE_TITLE'))->subject(trans("admin.New Agent registeration") . ' ' . env('SITE_TITLE'));
            });
        }
        catch (Exception $e) {
            // echo 'Caught exception: ', $e->getMessage(), "\n";
        }
        //////////////////////////////////////////
        flash()->success(trans('front.Registeration successfull and an email sent to your account for confirmation'));
        return back();
    }
}