<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libs\ACL;
use App\Libs\Adminauth;
use Request;
use Session;
use Form;
use App;

class Administrator extends Controller {

    public function __construct() {
        if (!Session::get('language')) {
            Session::put('language', 'english');
        }
        $this->middleware('AdminAuthenticate');
       // $this->middleware('AclAuthenticate', ['except' => ['getDeleteImage']]);
    }
}