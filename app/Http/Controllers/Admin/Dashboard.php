<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Administrator;
use App\Libs\ACL;
use App\Libs\Adminauth;
use App\Http\Controllers\Admin;
use DB;

class Dashboard extends Administrator {

    public function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        return view('admin.dashboard.index');
    }
}