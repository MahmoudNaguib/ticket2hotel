<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Libs\ACL;
use App\Libs\Adminauth;
use Config;
use App\Models\Contact;

class Contacts extends Administrator {

    public $model;
    public $module;

    public function __construct(Contact $model) {
        parent::__construct();
        $this->module = 'contacts';
        $this->model = $model;
    }

    public function getIndex(Request $request) {
        authorize('view-'.$this->module);
        $rows = $this->model->orderBy("viewed",'asc');
        $rows = $rows->get();
        return view('admin.' . $this->module . '.index',
            ['rows' => $rows, 'module' => $this->module]);
    }

    public function getView($id) {
        authorize('view-'.$this->module);
        $row = $this->model->findOrFail($id);
        $row->viewed=1;
        $row->save();
        return view('admin.' . $this->module . '.view',
            ['row' => $row, 'module' => $this->module]);
    }

    public function getDelete($id) {
        authorize('delete-'.$this->module);
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('admin.Delete successfull'));
        return back();
    }
    public function getExport() {
        authorize('export-' . $this->module);
        $titles = [
            "Name",
            "Email",
            "Mobile",
            "Subject",
            "Message",
        ];
        $output='';
        $output.=  implode(', ', $titles).PHP_EOL;
        $items = [];
        $rows = $this->model->latest()->get();
        if ($rows) {
            foreach ($rows as $row) {
                $item = [
                    'name' => $row->name,
                    'email' => @$row->email,
                    'mobile' => @$row->mobile,
                    'subject' => $row->subject,
                    'message' => $row->message,
                ];
                $output.=implode(', ', $item).PHP_EOL;
            }
        }
        header("Content-type: text/x-csv");
header("Content-Disposition: attachment; filename=".$this->module . "_" . date("Y-m-d").".csv");
echo($output);exit;
    }
}