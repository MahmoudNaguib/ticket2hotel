<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Libs\ACL;
use App\Libs\Adminauth;
use Config;
use App\Models\User;
use Session;
use Mail;

class Users extends Administrator {

    public $model;
    public $module;
    public $rules;

    public function __construct(User $model) {
        parent::__construct();
        $this->module = 'users';
        $this->model = $model;
    }

    public function getIndex(Request $request) {
        authorize('view-' . $this->module);
        $rows = $this->model->latest();
        $rows = $rows->get();
        return view('admin.' . $this->module . '.index',
            ['rows' => $rows, 'module' => $this->module]);
    }

    public function getView($id) {
        authorize('view-' . $this->module);
        $row = $this->model->findOrFail($id);
        return view('admin.' . $this->module . '.view',
            ['row' => $row, 'module' => $this->module]);
    }

    public function getDelete($id) {
        authorize('delete-' . $this->module);
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('admin.Delete successfull'));
        return back();
    }

    public function getPublish($value, $id) {
        authorize('publish-' . $this->module);
        $row = $this->model->findOrFail($id);
        if ($value == 0) {
            $row->published = 0;
            $published = trans('admin.Unpublished');
        }
        else {
            $row->published = 1;
            $published = trans('admin.Published');
        }
        $row->save();
        flash()->success($published . " " . trans('admin.Successfull'));
        return back();
    }
}