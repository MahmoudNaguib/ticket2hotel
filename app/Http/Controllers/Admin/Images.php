<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Libs\ACL;
use App\Libs\Adminauth;
use Config;
use App\Models\Image;

class Images extends Administrator {

    public $model;
    public $module;
    public $rules;

    public function __construct(Image $model, Request $request) {
        parent::__construct();
        $this->module = 'images';
        $this->model = $model;
    }

    public function getDeleteImage($id) {
        $row = $this->model->whereImageName($id)->orWhere("id", "=", $id)->first();
        if ($row)
            $row->delete();
        flash()->success(trans('admin.Delete successfull'));
        return back();
    }

}
