<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Libs\ACL;
use App\Libs\Adminauth;
use Config;
use App\Models\Role;

class Roles extends Administrator {

    public $model;
    public $module;
    public $rules;

    public function __construct(Role $model) {
        parent::__construct();
        $this->module = 'roles';
        $this->model = $model;
    }

    public function getIndex(Request $request) {
        authorizeSuperAdmin();
        $rows = $this->model->latest();
        $rows = $rows->get();
        return view('admin.' . $this->module . '.index',
            ['rows' => $rows, 'module' => $this->module]);
    }

    public function getView($id) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        return view('admin.' . $this->module . '.view',
            ['row' => $row, 'module' => $this->module]);
    }

    public function getAdd($table) {
        authorizeSuperAdmin();
        $this->model->create([
            'title' => "Create " . $table,
            'slug' => "create-" . $table,
            'category' => ucfirst($table),
        ]);
        $this->model->create([
            'title' => "Edit " . $table,
            'slug' => "edit-" . $table,
            'category' => ucfirst($table),
        ]);
        $this->model->create([
            'title' => "Delete " . $table,
            'slug' => "delete-" . $table,
            'category' => ucfirst($table),
        ]);
        $this->model->create([
            'title' => "View/List " . $table,
            'slug' => "view-" . $table,
            'category' => ucfirst($table),
        ]);
        $this->model->create([
            'title' => "Publish " . $table,
            'slug' => "publish-" . $table,
            'category' => ucfirst($table),
        ]);
        echo $table . " created";
    }

    public function getCreate() {
        authorizeSuperAdmin();
        $row = $this->model;
        return view('admin.' . $this->module . '.create',
            ['row' => $row, 'module' => $this->module]);
    }

    public function postCreate(Request $request) {
        authorizeSuperAdmin();
        $this->validate($request,
            ['title' => 'required|unique:roles,title', 'slug' => 'required|unique:roles,slug']);
        $row = $this->model->create($request->all());
        flash()->success(trans('admin.Add successfull'));
        return redirect('admin/' . $this->module . '');
    }

    public function getEdit($id) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        return view('admin.' . $this->module . '.edit',
            ['row' => $row, 'module' => $this->module]);
    }

    public function postEdit($id, Request $request) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        $this->validate($request,
            ['title' => 'required|unique:roles,title,' . $row->id, 'slug' => 'required|unique:roles,slug,' . $row->id]);
        $row->update($request->all());
        flash()->success(trans('admin.Edit successfull'));
        return redirect('admin/' . $this->module . '/edit/' . $row->id);
    }

    public function getDelete($id) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('admin.Delete successfull'));
        return back();
    }

    public function postDelete(Request $request) {
        authorizeSuperAdmin();
        $ids = $request->input("id");
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        $object = $this->model->whereIn("id", $ids)->delete();
        flash()->success(trans('admin.Delete successfull'));
        return back();
    }
}