<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Libs\ACL;
use App\Libs\Adminauth;
use Config;
use App\Models\Admin;
use Session;
use Mail;
use Hash;

class Admins extends Administrator {

    public $model;
    public $module;
    public $rules;

    public function __construct(Admin $model) {
        parent::__construct();
        $this->module = 'admins';
        $this->model = $model;
        $this->rules = [
            'name' => 'required',
            'email' => 'required|email|unique:admins,email',
            "password" => "required|confirmed|min:4",
        ];
    }

    public function getIndex(Request $request) {
        authorizeSuperAdmin();
        $rows = $this->model->whereSuperAdmin(0)->where('id','!=',1)->latest();
        $rows = $rows->get();
        return view('admin.' . $this->module . '.index', ['rows' => $rows, 'module' => $this->module]);
    }

    public function getView($id) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        return view('admin.' . $this->module . '.view', ['row' => $row, 'module' => $this->module]);
    }

    public function getCreate() {
        authorizeSuperAdmin();
        $row = $this->model;
        $row->published=1;
        $roles = \App\Models\Role::all();
        return view('admin.' . $this->module . '.create', ['row' => $row, 'module' => $this->module, 'roles' => $roles]);
    }

    public function postCreate(Request $request) {
        authorizeSuperAdmin();
        $this->validate($request, $this->rules);
        if ($row = $this->model->create($request->all())) {
            //////////////////////////////////////send confirm email
            $configs = Session::get('configs');
            try {
                Mail::send('emails.admins.confirm', ['row' => $row, 'configs' => $configs], function ($mail) use ($row, $configs) {
                    $mail->to($row->email, $row->name)->subject(trans("admin.Welcome to") . ' ' . env('SITE_TITLE'));
                });
            } catch (Exception $e) {
                // echo 'Caught exception: ', $e->getMessage(), "\n";
            }
            //////////////////////////////////////////
            $row->roles()->sync((array) $request->input('role_list'));
            flash()->success(trans('admin.Add successfull'));
            return redirect('admin/' . $this->module . '');
        }
        flash()->error(trans('admin.failed to save'));
    }

    public function getEdit($id) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        $roles = \App\Models\Role::all();
        return view('admin.' . $this->module . '.edit', ['row' => $row, 'module' => $this->module, 'roles' => $roles]);
    }

    public function postEdit($id, Request $request) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        $this->rules['email'] = $this->rules['email'] . "," . $row->id;
        $this->rules['password'] = "confirmed|min:4";
        $this->validate($request, $this->rules);
        if ($row->update($request->except(['password']))) {
            if (trim($request->input('password'))) {
                $row->password = trim($request->input('password'));
                $row->save();
            }
            $row->roles()->sync((array) $request->input('role_list'));
            flash()->success(trans('admin.Edit successfull'));
            return redirect('admin/' . $this->module . '/edit/' . $row->id);
        }
        flash()->error(trans('admin.failed to save'));
    }

    public function getDelete($id) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('admin.Delete successfull'));
        return back();
    }
    public function getPublish($value, $id) {
        authorizeSuperAdmin();
        $row = $this->model->findOrFail($id);
        if ($value == 0) {
            $row->published = 0;
            $published = trans('admin.Unpublished');
        } else {
            $row->published = 1;
            $published = trans('admin.Published');
        }
        $row->save();
        flash()->success($published . " " . trans('admin.Successfull'));
        return back();
    }

    public function getChangePassword() {
        $row = $this->model->findOrFail(Adminauth::user()->id);
        return view('admin.' . $this->module . '.change-password', ['row' => $row, 'module' => $this->module]);
    }

    public function postChangePassword(Request $request) {
        $row = $this->model->findOrFail(Adminauth::user()->id);
        $rules['password'] = 'required|confirmed|min:4';
        $this->validate($request, $rules);
        if (!Hash::check(trim($request->input('old_password')), $row->password)) {
            flash()->error(trans('admin.Check your old password'));
            return back()->withInput();
        }
        if (trim($request->input('password'))) {
            $row->password = trim($request->input('password'));
            $row->save();
        }
        flash()->success(trans('admin.Password has been changed'));
        return redirect('admin/' . $this->module . '/change-password');
    }

    public function getEditAccount() {
        $row = $this->model->findOrFail(Adminauth::user()->id);
        return view('admin.' . $this->module . '.edit-account', ['row' => $row, 'module' => $this->module]);
    }

    public function postEditAccount(Request $request) {
        $row = $this->model->findOrFail(Adminauth::user()->id);
        $rules['name'] = "required";
        $rules['email'] = 'required|email|unique:admins,email' . "," . $row->id;
        $this->validate($request, $rules);
        if ($row->update($request->except(['password', 'published']))) {
            flash()->success(trans('admin.Edit successfull'));
            return redirect('admin/' . $this->module . '/edit-account');
        }
        flash()->error(trans('admin.failed to save'));
    }

}
