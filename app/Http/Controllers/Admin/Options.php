<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Libs\ACL;
use App\Libs\Adminauth;
use Config;

class Options extends Administrator {

    public $model;
    public $module;
    public $rules;

    public function __construct(\App\Models\Option $model, Request $request) {
        parent::__construct();
        $this->module = 'options';
        $this->model = $model;
        $this->rules = [
            'type' => 'required',
            'title' => 'required',
        ];
    }

    public function getIndex(Request $request) {
        authorize('view-' . $this->module);
        $rows = $this->model->latest();
        $rows = $rows->get();
        return view('admin.' . $this->module . '.index',
            ['rows' => $rows, 'module' => $this->module]);
    }

    public function getView($id) {
        authorize('view-' . $this->module);
        $row = $this->model->findOrFail($id);
        return view('admin.' . $this->module . '.view',
            ['row' => $row, 'module' => $this->module]);
    }

    public function getCreate() {
        authorize('create-' . $this->module);
        $row = $this->model;
        $row->published = 1;
        return view('admin.' . $this->module . '.create',
            ['row' => $row, 'module' => $this->module]);
    }

    public function postCreate(Request $request) {
        authorize('create-' . $this->module);
        $this->validate($request, $this->rules);
        if ($row = $this->model->create($request->all())) {
            flash()->success(trans('admin.Add successfull'));
            return redirect('admin/' . $this->module . '');
        }
        flash()->error(trans('admin.failed to save'));
    }

    public function getEdit($id) {
        authorize('edit-' . $this->module);
        $row = $this->model->findOrFail($id);
        return view('admin.' . $this->module . '.edit',
            ['row' => $row, 'module' => $this->module]);
    }

    public function postEdit($id, Request $request) {
        authorize('edit-' . $this->module);
        $row = $this->model->findOrFail($id);
        $this->validate($request, $this->rules);
        if ($row->update($request->all())) {
            flash()->success(trans('admin.Edit successfull'));
            return back();
        }
        flash()->error(trans('admin.failed to save'));
    }

    public function getPublish($value, $id) {
        authorize('publish-' . $this->module);
        $row = $this->model->findOrFail($id);
        if ($value == 0) {
            $row->published = 0;
            $published = trans('admin.Unpublished');
        }
        else {
            $row->published = 1;
            $published = trans('admin.Published');
        }
        $row->save();
        flash()->success($published . " " . trans('admin.Successfull'));
        return back();
    }
}