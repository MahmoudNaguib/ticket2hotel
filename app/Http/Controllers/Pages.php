<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front;
use Config;

class Pages extends Front {

    public function __construct(\App\Models\Page $model) {
        parent::__construct();
        $this->module = 'pages';
        $this->model = $model;
    }

    public function home() {
        $row = $this->model->findOrFail(1);
        $page_title = $row->title;
        $meta_description = str_limit($row->meta_description, 190);
        $meta_keywords = str_limit($row->meta_keywords, 190);
        if ($row->main_image) {
            $main_image =$row->main_image;
        }
        return view('front.'.$this->module .'.home',
            compact('row', 'page_title', 'meta_description', 'meta_keywords',
                'main_image'));
    }
    public function aboutUs() {
        $row = $this->model->findOrFail(2);
        $page_title = $row->title;
        $meta_description = str_limit($row->meta_description, 190);
        $meta_keywords = str_limit($row->meta_keywords, 190);
        if ($row->main_image) {
            $main_image =$row->main_image;
        }
        return view('front.'.$this->module .'.about-us',
            compact('row', 'page_title', 'meta_description', 'meta_keywords',
                'main_image'));
    }
    public function contactUs() {
        $row = $this->model->findOrFail(3);
        $page_title = $row->title;
        $meta_description = str_limit($row->meta_description, 190);
        $meta_keywords = str_limit($row->meta_keywords, 190);
        if ($row->main_image) {
            $main_image =$row->main_image;
        }
        return view('front.'.$this->module .'.contact-us',
            compact('row', 'page_title', 'meta_description', 'meta_keywords',
                'main_image'));
    }
    public function termsAndConditions() {
        $row = $this->model->findOrFail(4);
        $page_title = $row->title;
        $meta_description = str_limit($row->meta_description, 190);
        $meta_keywords = str_limit($row->meta_keywords, 190);
        if ($row->main_image) {
            $main_image =$row->main_image;
        }
        return view('front.'.$this->module .'.terms-and-conditions',
            compact('row', 'page_title', 'meta_description', 'meta_keywords',
                'main_image'));
    }
}