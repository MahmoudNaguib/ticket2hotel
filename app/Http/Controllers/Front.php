<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use Session;
use Form;
use App;

class Front extends Controller {

    public function __construct() {
        ///////////////////
        if (!Session::has('language')) {
            Session::put('language', env('DEFAULT_LANG'));
        }
        if (!Session::has('configs')) {
            $configs = getConfigs();
            Session::put('configs', $configs);
        }
        ///////////////////////
        /////////////////////// Localization
        if (@Request::input("lang")) {
            $lang = Request::input("lang");
            if ($lang == 'arabic') {
                Session::set("language", $lang);
                App::setLocale('ar');
            } else {
                Session::set("language", $lang);
                App::setLocale('en');
            }
        }
        if (Session::get('language') == "english") {
            App::setLocale('en');
        } else {
            App::setLocale('ar');
        }
        ///////////////////////////////////////// end Localization
    }

}
