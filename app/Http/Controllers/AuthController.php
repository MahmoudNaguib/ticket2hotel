<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front;
use Config;
use App\Models\User;
use Session;
use Mail;
use Auth;

class AuthController extends Front {

    public $model;
    public $module;

    public function __construct(User $model) {
        $this->middleware('IsAuth', ['except' => ['getLogout']]);
        $this->module = 'auth';
        $this->model = $model;
    }

    public function getRegister() {
        $page_title = trans("front.Register");
        $row = $this->model;
        return view('front.' . $this->module . '.register', ['row' => $row, 'module' => $this->module, 'page_title' => $page_title]);
    }

    public function postRegister(Request $request) {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required|phone',
            "password" => "required|confirmed|min:4",
        ];
        $this->validate($request, $rules);
        $row = $this->model->create($request->except(['password_confirmation']));
        ////////////////////////////////////// update guarded fields
        $row->confirm_token = md5(time()) . md5($request->input('email'));
        $row->published = 1;
        $row->save();
        //////////////////////////////////////send confirm email
        $configs = Session::get('configs');
        try {
            Mail::send('emails.users.confirm', ['row' => $row, 'configs' => $configs], function ($mail) use ($row, $configs) {
                $mail->to($row->email, $row->name)->subject(trans("admin.Welcome to") . ' ' . env('SITE_TITLE'));
            });
        } catch (Exception $e) {
            // echo 'Caught exception: ', $e->getMessage(), "\n";
        }
        //////////////////////////////////////////
        flash()->success(trans('front.Registeration successfull and an email sent to your account for confirmation'));
        return back();
    }

    public function getActivate($token) {
        $row = User::where('confirm_token', '=', $token)->first();
        if (!$row) {
            flash()->error(trans('front.Invalid user link'));
            return redirect('/');
        }
        $row->confirmed = 1;
        $row->save();
        flash()->success(trans('front.User has been confirmed'));
        return redirect('/');
    }

    public function getLogin() {
        $page_title = trans("front.Login");
        $row = $this->model;
        return view('front.' . $this->module . '.login', ['row' => $row, 'module' => $this->module, 'page_title' => $page_title]);
    }

    public function postLogin(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:4',
        ]);
        $row = $this->model->whereEmail($request->input('email'))->first();
        if (!$row) {
            flash()->error(trans('front.This email is not exist'));
            return back()->withInput();
        } else {
            if ($row->published == 0) {
                flash()->error(trans('front.Acccount with this email is banned'));
                return back()->withInput();
            }
            if ($row->confirmed == 0) {
                flash()->error(trans('front.Acccount with this email is not confirmed, Please go to your email to confirm your account'));
                return back()->withInput();
            }
            if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'),
                        'confirmed' => 1, 'published' => 1])) {
                return redirect('/');
            } else {
                flash()->error(trans('front.Invalid email or password'));
                return back()->withInput();
            }
        }
    }

    public function getForgotPassword() {
        $page_title = trans("front.forgot password");
        $row = $this->model;
        return view('front.' . $this->module . '.forgot-password', ['row' => $row, 'module' => $this->module, 'page_title' => $page_title]);
    }

    public function postForgotPassword(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
        ]);
        $row = $this->model->whereEmail($request->input('email'))->first();
        if (!$row) {
            flash()->error(trans('front.This email is not exist'));
            return back()->withInput();
        } else {
            $password = str_random(10);
            $row->password = $password;
            $row->save();
            $configs = Session::get('configs');
            try {
                Mail::send('emails.users.password', ['row' => $row, 'configs' => $configs, 'password' => $password], function ($mail) use ($row, $configs) {
                    $mail->to($row->email, $row->name)->subject('Your new password at ' . env('SITE_TITLE'));
                });
            } catch (Exception $e) {
                // echo 'Caught exception: ', $e->getMessage(), "\n";
            }
            flash()->success(trans('front.New password has been sent to your email'));
            return back();
        }
    }

    public function getLogout() {
        Auth::logout();
        return redirect('/');
    }

}
