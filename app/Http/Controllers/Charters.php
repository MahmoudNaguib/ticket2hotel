<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front;
use Config;
use App\Models\User;
use Session;
use Mail;
use Auth;

class Charters extends Front {

    public $model;
    public $module;

    public function __construct(\App\Models\Charter $model) {
        parent::__construct();
        $this->module = 'charters';
        $this->model = $model;
    }

    public function getRequest() {
        $row = \App\Models\Page::findOrFail(7);
        $page_title = $row->title;
        $meta_description = str_limit($row->meta_description, 190);
        $meta_keywords = str_limit($row->meta_keywords, 190);
        if ($row->main_image) {
            $main_image = $row->main_image;
        }
        $row = $this->model;
        return view('front.' . $this->module . '.request',
            compact('row', 'page_title', 'meta_description', 'meta_keywords',
                'main_image'));
    }

    public function postRequest(Request $request) {
        $rules = [
            'type' => 'required',
            'no_of_passengers' => 'required_if:type,1,3',
            'total_weight' => 'required_if:type,2,3',
            //'no_of_passengers'=>'required',
            'from' => 'required',
            'to' => 'required',
            'prefered_etd' => 'required',
            'notes' => 'required',
            'contact_email' => 'required|email',
            'contact_mobile' => 'required',
            'contact_name' => 'required',
            'date_of_operation' => 'required'
        ];
        $this->validate($request, $rules);
        $row = $this->model->create($request->all());
        $configs = Session::get('configs');

        $to_array = explode(',', $configs['request_charter_form']);
        if ($to_array) {
            foreach ($to_array as $to_email) {
                try {
                    Mail::send('emails.charters.request',
                        ['row' => $row, 'configs' => $configs],
                        function ($mail) use ($row, $to_email, $configs) {
                        $mail->to(trim($to_email), env('SITE_TITLE'))->subject(trans("admin.New Request charter") . ' ' . env('SITE_TITLE'));
                    });
                }
                catch (Exception $e) {
                    // echo 'Caught exception: ', $e->getMessage(), "\n";
                }
            }
        }

        //////////////////////////////////////////
        flash()->success(trans('front.Your request has been sent'));
        return back();
    }
}