<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front;
use Config;
use App\Models\Contact;
use Session;
use Mail;

class Contacts extends Front {

    public function __construct() {
        parent::__construct();
    }

    public function postSend(Request $request) {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ];
        $configs = Session::get('configs');
        $this->validate($request, $rules);
        $row = Contact::create($request->all());
        if (@$configs['contact_form_receive_email']) {
            try {
                Mail::send('emails.contacts.send',
                    ['row' => $row, 'configs' => $configs],
                    function ($mail) use ($row, $configs, $request) {
                    $subject = trans('front.Contact message from') . ' ' . env('SITE_TITLE');
                    $mail->to(@$configs['contact_form_receive_email'],
                        env('SITE_TITLE') . ' - ' . trans('front.Administrator'))->subject($subject);
                });
            }
            catch (Exception $e) {
                //  echo 'Caught exception: ', $e->getMessage(), "\n";
            }
        }

        flash()->success(trans('front.Your message has been sent'));
        return back();
    }
}