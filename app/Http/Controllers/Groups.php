<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front;
use Config;
use App\Models\User;
use Session;
use Mail;
use Auth;

class Groups extends Front {

    public $model;
    public $module;

    public function __construct(\App\Models\Group $model) {
        parent::__construct();
        $this->module = 'groups';
        $this->model = $model;
    }

    public function getRequest() {
        $row = \App\Models\Page::findOrFail(8);
        $page_title = $row->title;
        $meta_description = str_limit($row->meta_description, 190);
        $meta_keywords = str_limit($row->meta_keywords, 190);
        if ($row->main_image) {
            $main_image = $row->main_image;
        }
        $row = $this->model;
        return view('front.' . $this->module . '.request',
            compact('row', 'page_title', 'meta_description', 'meta_keywords',
                'main_image'));
    }

    public function postRequest(Request $request) {
        $rules = [
            'group_name' => 'required',
            'destination' => 'required',
            'group_type' => 'required',
            'contact_email' => 'required|email',
            'contact_mobile' => 'required',
            'contact_name' => 'required',
        ];
        $this->validate($request, $rules);
       // dd($request->all());
        $row = $this->model->create(jsonRequest($request->all()));
        $configs = Session::get('configs');
        $to_array = explode(',', $configs['group_form']);
        if ($to_array) {
            foreach ($to_array as $to_email) {
                try {
                    Mail::send('emails.groups.request',
                        ['row' => $row, 'configs' => $configs],
                        function ($mail) use ($row, $to_email, $configs) {
                        $mail->to(trim($to_email), env('SITE_TITLE'))->subject(trans("admin.New Group Request") . ' ' . env('SITE_TITLE'));
                    });
                }
                catch (Exception $e) {
                    // echo 'Caught exception: ', $e->getMessage(), "\n";
                }
            }
        }
        //////////////////////////////////////////
        flash()->success(trans('front.Your request has been sent'));
        return back();
    }
}